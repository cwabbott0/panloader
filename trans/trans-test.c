#define _LARGEFILE64_SOURCE 1
#define CACHE_LINE_SIZE 1024 /* TODO */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include "pandev.h"
#include "trans-builder.h"

int main(int argc, const char **argv)
{
	struct pipe_screen _screen;
	struct pipe_screen *screen = &_screen;
	struct pipe_context *gallium = panfrost_create_context(screen, NULL, 0);

	struct pipe_shader_state vs_cso, fs_cso;

	gallium->bind_fs_state(gallium, gallium->create_fs_state(gallium, &fs_cso));
	gallium->bind_vs_state(gallium, gallium->create_vs_state(gallium, &vs_cso));

	float attributes_data_1_0[] = {
            -0.50000f, 0.500000f, 0.000000f, 1.0f,
            0.500000f, 0.500000f, 0.000000f, 1.0f,
            -0.50000f, -0.500000f, 0.000000f, 1.0f,
            0.500000f, -0.500000f, 0.000000f, 1.0f,

            -0.50000f, 0.500000f, 0.000000f, 1.0f,
            0.500000f, 0.500000f, 0.000000f, 1.0f,
            -0.50000f, -0.500000f, 0.000000f, 1.0f,
            0.500000f, -0.500000f, 0.000000f, 1.0f,

        };

	struct pipe_resource templ = {
		.width0 = sizeof(attributes_data_1_0)
	};

	struct pipe_box box = {};

	struct pipe_resource *rsrc = screen->resource_create(screen, &templ);

	struct pipe_transfer *transfer;
	uint8_t *attrib_trans = gallium->transfer_map(gallium, rsrc, 0, 0, &box, &transfer);

	const struct pipe_vertex_element attrs[] = {
		{
			/* TODO */
		},
		{}
	};

	gallium->bind_vertex_elements_state(gallium, gallium->create_vertex_elements_state(gallium, 2, attrs));

	const struct pipe_vertex_buffer buffs[] ={
	       	{
			.buffer = { .resource = rsrc }
		},
	       	{
			.buffer = { .resource = rsrc }
		},
	};

	gallium->set_vertex_buffers(gallium, 0, 2, buffs);

        float uniforms[] = {
		0.8, 0.0, 0.4, 1.0,
		0.2, 0.9, 0.6, 1.0,
		0.1, 0.1, 0.2, 1.0,
		0.3, 0.7, 0.1, 1.0,
        };
	templ.width0 = sizeof(uniforms);

	struct pipe_transfer *transfer2;
	struct pipe_resource *cbuf = screen->resource_create(screen, &templ);
	memcpy(gallium->transfer_map(gallium, cbuf, 0, 0, &box, &transfer2), uniforms, sizeof(uniforms));

	struct pipe_constant_buffer consts = {
		.buffer = cbuf,
		.buffer_size = sizeof(uniforms)
	};
	
	gallium->set_constant_buffer(gallium, PIPE_SHADER_VERTEX, 0, &consts);

	memcpy(attrib_trans,
		attributes_data_1_0,
		sizeof(attributes_data_1_0));

	const struct pipe_rasterizer_state state = {
		.line_width = 10.0f,
		.front_ccw = false,
		.multisample = true
	};

	gallium->bind_rasterizer_state(gallium,
			gallium->create_rasterizer_state(gallium, &state));

    for (int i = 0; i < 600; ++i) {
	const struct pipe_rasterizer_state stat = {
		.line_width = 10.0f,
		.front_ccw = false,
		.multisample = true
	};

	gallium->bind_rasterizer_state(gallium,
			gallium->create_rasterizer_state(gallium, &stat));

	attributes_data_1_0[0] = 0.01f * ((float) i);

	memcpy(attrib_trans,
		attributes_data_1_0,
		sizeof(attributes_data_1_0));

        union pipe_color_union u = { .f = { 0.1, 0.1, 0.1, 1.0 } };
        gallium->clear(gallium, PIPE_CLEAR_COLOR | PIPE_CLEAR_DEPTH | PIPE_CLEAR_STENCIL, &u, 0.0, 0.0);

	struct pipe_draw_info info = {
		.start = 0,
		.count = 3,
		.mode = PIPE_PRIM_LINE_LOOP
	};

	gallium->draw_vbo(gallium, &info);

	/*
	attributes_data_1_0[0] = 0;

	memcpy(attrib_trans,
		attributes_data_1_0,
		sizeof(attributes_data_1_0));
		*/

	const struct pipe_rasterizer_state staate = {
		.line_width = 1.0f,
		.front_ccw = false,
		.multisample = true
	};

	info.mode = PIPE_PRIM_LINE_STRIP;

	gallium->bind_rasterizer_state(gallium,
			gallium->create_rasterizer_state(gallium, &staate));


	gallium->draw_vbo(gallium, &info);

	gallium->flush(gallium, NULL, PIPE_FLUSH_END_OF_FRAME);
    }

    gallium->transfer_unmap(gallium, transfer);

    return 0;
}
