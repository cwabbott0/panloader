/*
 * © Copyright 2018 The Panfrost Community
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#include "trans-builder.h"

/* The trans builder, as used in the _trans_itory driver (hence the name,
 * obviously!), exposes a thin layer over the hardware. It exposes a low-level
 * API for building up the GPU memory data structures based on state. It
 * explicitly does _not_ attempt to model OpenGL or other high-level APIs. */

/* MSAA is not supported in sw_winsys but it does make for nicer demos ;) so we
 * can force it regardless of gallium saying we don't have it */
static bool FORCE_MSAA = true;

/* Descriptor is generated along with the shader compiler */

static void
trans_upload_varyings_descriptor(struct panfrost_context *ctx)
{
        mali_ptr unknown6_1_p = panfrost_upload(&ctx->cmdstream, &ctx->varyings_descriptor_0, sizeof(struct mali_unknown6), true);
        mali_ptr unknown6_2_p = panfrost_upload_sequential(&ctx->cmdstream, &ctx->varyings_descriptor_1, sizeof(struct mali_unknown6));
        ctx->payload_vertex.unknown6 = (unknown6_1_p) | 0x0;
        ctx->payload_tiler.unknown6 = (unknown6_2_p) | 0x8;
}

/* TODO: Sample size, etc */

static void
trans_set_framebuffer_msaa(struct panfrost_context *ctx, bool enabled)
{
	if (enabled) {
		ctx->fragment_shader_core.unknown2_3 |= MALI_HAS_MSAA;
		ctx->fragment_shader_core.unknown2_4 &= ~MALI_NO_MSAA;
		ctx->fragment_fbd.unknown2 |= MALI_FRAMEBUFFER_MSAA_A | MALI_FRAMEBUFFER_MSAA_B;
	} else {
		ctx->fragment_shader_core.unknown2_3 &= ~MALI_HAS_MSAA;
		ctx->fragment_shader_core.unknown2_4 |= MALI_NO_MSAA;
		ctx->fragment_fbd.unknown2 &= ~(MALI_FRAMEBUFFER_MSAA_A | MALI_FRAMEBUFFER_MSAA_B);
	}
}

/* Framebuffer descriptor */

/* No idea why this is needed, but it's how resolution_check is calculcated.
 * It's not clear to us yet why the hardware wants this. The formula itself was
 * discovered mostly by manual bruteforce... don't envy me, guys */

static int
trans_compute_fb_resolution_check(int width, int height)
{
	/* Round each dimension to the nearest multiple of 64 */
	int rw = 64 * (int) round((float) width / 64.0);
	int rh = 64 * (int) round((float) height / 64.0);

	return 0x200 * ((rw + rh) / 96);
}

static void
trans_set_framebuffer_resolution(struct mali_single_framebuffer *fb, int w, int h)
{
	fb->width = MALI_POSITIVE(w);
	fb->height = MALI_POSITIVE(h);
	fb->resolution_check = trans_compute_fb_resolution_check(w, h);
}

struct mali_single_framebuffer
trans_emit_fbd(struct panfrost_context *ctx)
{
    struct mali_single_framebuffer framebuffer = {
	    .flags = 0x1f,
	    .unknown2 = 0x30000000,
	    .clear_flags = 0x1000,
	    .unknown_address_0 = ctx->scratchpad.gpu,
	    .unknown_address_1 = ctx->scratchpad.gpu + 0x6000,
	    .unknown_address_2 = ctx->scratchpad.gpu + 0x6200,
	    .unknown9 = 0xf0,
	    .tiler_heap_free = ctx->tiler_heap.gpu,
	    .tiler_heap_end = ctx->tiler_heap.gpu + ctx->tiler_heap.size,
    };

    trans_set_framebuffer_resolution(&framebuffer, ctx->width, ctx->height);

    return framebuffer;
}

/* The above function is for generalised fbd emission, used in both fragment as
 * well as vertex/tiler payloads. This payload is specific to fragment
 * payloads. */

static void
trans_new_frag_framebuffer(struct panfrost_context *ctx)
{
        struct mali_single_framebuffer fb = trans_emit_fbd(ctx);

	/* Framebuffer start is computed relative to the end */
        fb.framebuffer_end = ctx->framebuffer.gpu + ctx->framebuffer.size;
	fb.negative_stride = -ctx->stride;

        fb.unknown2 = 0xb84e0281;

        memcpy(&ctx->fragment_fbd, &fb, sizeof(fb));
}

/* Helpers used by the below functions */

/* Maps float 0.0-1.0 to int 0x00-0xFF */
static uint8_t
normalised_float_to_u8(float f) {
	return (uint8_t) (int) (f * 255.0f);
}

static void
panfrost_clear(
		struct pipe_context *pipe,
		unsigned buffers,
		const union pipe_color_union *color,
		double depth, unsigned stencil)
{
	struct panfrost_context *ctx = panfrost_context(pipe);
	struct mali_single_framebuffer *fbd = &ctx->fragment_fbd;

	/* Remember that we've done something */
	ctx->dirty |= PAN_DIRTY_DUMMY;

	bool clear_color = buffers & PIPE_CLEAR_COLOR;
	bool clear_depth = buffers & PIPE_CLEAR_DEPTH;
	bool clear_stencil = false && buffers & PIPE_CLEAR_STENCIL;

	uint32_t packed_color =
		(normalised_float_to_u8(/* color->f[3] */ 1.0f) << 24) |
		(normalised_float_to_u8(color->f[2]) << 16) |
		(normalised_float_to_u8(color->f[1]) <<  8) |
		(normalised_float_to_u8(color->f[0]) <<  0);

	/* Fields duplicated 4x for unknown reasons. Same in Utgard, too, which
	 * is doubly weird. */

	if (clear_color) {
		fbd->clear_color_1 = packed_color;
		fbd->clear_color_2 = packed_color;
		fbd->clear_color_3 = packed_color;
		fbd->clear_color_4 = packed_color;
	}

	if (clear_depth) {
		fbd->depth_buffer = ctx->depth_stencil_buffer;
		fbd->depth_buffer_enable = MALI_DEPTH_STENCIL_ENABLE;

		fbd->clear_depth_1 = depth;
		fbd->clear_depth_2 = depth;
		fbd->clear_depth_3 = depth;
		fbd->clear_depth_4 = depth;
	}

	if (clear_stencil) {
		fbd->stencil_buffer = ctx->depth_stencil_buffer;
		fbd->stencil_buffer_enable = MALI_DEPTH_STENCIL_ENABLE;

		fbd->clear_stencil = stencil;
	}

	/* Set flags based on what has been cleared */
	/* XXX: What do these flags mean? */
	int clear_flags = 0x101100;

	if (clear_color && clear_depth && clear_stencil) {
		/* On a tiler like this, it's fastest to clear all three buffers at once */

		clear_flags |= MALI_CLEAR_FAST;
	} else {
		clear_flags |= MALI_CLEAR_SLOW;

		if (clear_stencil)
			clear_flags |= MALI_CLEAR_SLOW_STENCIL;
	}

	fbd->clear_flags = clear_flags;
}



/* Reset per-frame context, called on context initialisation as well as after
 * flushing a frame */

static void
trans_invalidate_frame(struct panfrost_context *ctx)
{
	/* Reset varyings allocated */
	ctx->varying_height = 0;

	/* The cmdstream is dirty every frame; the only bits worth preserving
	 * (textures, shaders, etc) are in other buffers anyways */
	ctx->cmdstream.stack_bottom = 0;

	/* XXX: Shader binaries should be preserved with dirty tracking!!! */
	ctx->shaders.stack_bottom = 0;

	/* Regenerate payloads */
	trans_emit_vertex_payload(ctx);
        trans_emit_tiler_payload(ctx);
	trans_emit_vt_framebuffer(ctx);

        ctx->dirty |= PAN_DIRTY_VARYINGS;

	if (ctx->rasterizer)
		ctx->dirty |= PAN_DIRTY_RASTERIZER;

	/* XXX: We wouldn't need to upload this every frame if we were clever about tracking */
	trans_upload_varyings_descriptor(ctx);

	/* Viewport */
	/* XXX: Galliumify for realsies */

        trans_viewport(ctx, 0.0, 1.0, 0, 0, ctx->width, ctx->height);
	ctx->dirty |= PAN_DIRTY_VIEWPORT;

	/* Uniforms are all discarded with the above stack discard */

	for (int i = 0; i <= PIPE_SHADER_FRAGMENT; ++i)
		ctx->constant_buffer[i].dirty = true;

	/* XXX: Framebuffer update the right way */
	trans_new_frag_framebuffer(ctx);
}


void
trans_viewport(struct panfrost_context *ctx,
		float depth_range_n,
		float depth_range_f,
		int viewport_x0, int viewport_y0,
		int viewport_x1, int viewport_y1)
{
	/* Viewport encoding is asymmetric. Purpose of the floats is unknown? */

	struct mali_viewport ret = {
		.floats = {
			-inff, -inff,
			inff, inff,
		},

		.depth_range_n = depth_range_n, 
		.depth_range_f = depth_range_f,

		.viewport0 = { viewport_x0, viewport_y0 },
		.viewport1 = { MALI_POSITIVE(viewport_x1), MALI_POSITIVE(viewport_y1) },
	};

	memcpy(&ctx->viewport, &ret, sizeof(ret));
}

/* Shared descriptor for attributes as well as varyings, which can be
 * considered like a matrix. This function is just a simplified constructor; no
 * actual bookkeeping is done (hence the lack of a context parameter). */

struct mali_attr
trans_attr(size_t type_sz, int columns, int vertices)
{
	int stride = type_sz * columns;

	struct mali_attr ret = {
		.stride = stride,
		.size = stride * vertices
	};

	return ret;
}

/* In practice, every field of these payloads should be configurable
 * arbitrarily, which means these functions are basically catch-all's for
 * as-of-yet unwavering unknowns */

void
trans_emit_vertex_payload(struct panfrost_context *ctx)
{
	struct mali_payload_vertex_tiler payload = {
		.unk1 = 0x28000000,
		.unknown_draw = 0x1400000,
		.gl_enables = 0x4
	};

	/* Who knows why the GPU needs this?! */

        u32 inner_unknown = 0; 
        mali_ptr inner_unknown_p = panfrost_upload(&ctx->cmdstream, &inner_unknown, 4, false);
        u64 unknown = ((inner_unknown_p) << 8) | 1;
        payload.unknown1 = panfrost_upload(&ctx->cmdstream, &unknown, 8, false);

	memcpy(&ctx->payload_vertex, &payload, sizeof(payload));

	/* Shader, vertex elements are necessarily dirtied... since we just zeroed it in the struct */
	ctx->dirty |= PAN_DIRTY_VS | PAN_DIRTY_VERTEX | PAN_DIRTY_VERT_BUF;
}

void
trans_emit_tiler_payload(struct panfrost_context *ctx)
{
	struct mali_payload_vertex_tiler payload_1 = {
		.unk1 = 0x28000000,
		.unknown_draw = 0x1803800
	};

	memcpy(&ctx->payload_tiler, &payload_1, sizeof(payload_1));

	/* See above */
	ctx->dirty |= PAN_DIRTY_FS;
}

void
trans_emit_vt_framebuffer(struct panfrost_context *ctx)
{
	struct mali_single_framebuffer framebuffer_1 = trans_emit_fbd(ctx);
	mali_ptr framebuffer_1_p = panfrost_upload(&ctx->cmdstream, &framebuffer_1, sizeof(framebuffer_1), false);
	ctx->payload_vertex.framebuffer = framebuffer_1_p;
	ctx->payload_tiler.framebuffer = framebuffer_1_p;
}

static bool
trans_make_dominant_factor(unsigned src_factor, enum mali_dominant_factor *factor, bool *invert)
{
	switch (src_factor) {
		case PIPE_BLENDFACTOR_SRC_COLOR: 
		case PIPE_BLENDFACTOR_INV_SRC_COLOR: 
			*factor = MALI_DOMINANT_SRC_COLOR;
			break;

		case PIPE_BLENDFACTOR_SRC_ALPHA: 
		case PIPE_BLENDFACTOR_INV_SRC_ALPHA: 
			*factor = MALI_DOMINANT_SRC_ALPHA;
			break;

		case PIPE_BLENDFACTOR_DST_COLOR: 
		case PIPE_BLENDFACTOR_INV_DST_COLOR: 
			*factor = MALI_DOMINANT_DST_COLOR;
			break;

		case PIPE_BLENDFACTOR_DST_ALPHA: 
		case PIPE_BLENDFACTOR_INV_DST_ALPHA: 
			*factor = MALI_DOMINANT_DST_ALPHA;
			break;

		case PIPE_BLENDFACTOR_ONE: 
		case PIPE_BLENDFACTOR_ZERO: 
			*factor = MALI_DOMINANT_ZERO;
			break;

		case PIPE_BLENDFACTOR_SRC_ALPHA_SATURATE: 
		case PIPE_BLENDFACTOR_CONST_COLOR: 
		case PIPE_BLENDFACTOR_CONST_ALPHA: 
		case PIPE_BLENDFACTOR_SRC1_COLOR: 
		case PIPE_BLENDFACTOR_SRC1_ALPHA: 
		case PIPE_BLENDFACTOR_INV_CONST_COLOR: 
		case PIPE_BLENDFACTOR_INV_CONST_ALPHA: 
		case PIPE_BLENDFACTOR_INV_SRC1_COLOR: 
		case PIPE_BLENDFACTOR_INV_SRC1_ALPHA: 
		default:
			/* Fancy blend modes not supported */
			return false;
	}

	/* Set invert flags */

	switch (src_factor) {
		case PIPE_BLENDFACTOR_ONE:
		case PIPE_BLENDFACTOR_INV_SRC_COLOR: 
		case PIPE_BLENDFACTOR_INV_SRC_ALPHA: 
		case PIPE_BLENDFACTOR_INV_DST_ALPHA: 
		case PIPE_BLENDFACTOR_INV_DST_COLOR: 
		case PIPE_BLENDFACTOR_INV_SRC1_COLOR: 
		case PIPE_BLENDFACTOR_INV_SRC1_ALPHA: 
			*invert = true;
		default:
			break;
	}
	
	return true;
}

static bool
is_edge_blendfactor(unsigned factor) {
	return factor == PIPE_BLENDFACTOR_ONE || factor == PIPE_BLENDFACTOR_ZERO;
}

static bool
trans_make_fixed_blend_part(unsigned func, unsigned src_factor, unsigned dst_factor, unsigned *out)
{
	struct mali_blend_mode part = { 0 };

	/* Make sure that the blend function is representible with negate flags */

	if (func == PIPE_BLEND_ADD) {
		/* Default, no modifiers needed */
	} else if (func == PIPE_BLEND_SUBTRACT)
		part.negate_dest = true;
	else if (func == PIPE_BLEND_REVERSE_SUBTRACT)
		part.negate_source = true;
	else
		return false;

	part.clip_modifier = MALI_BLEND_MOD_NORMAL;

	/* Decide which is dominant, source or destination. If one is an edge
	 * case, use the other as a factor. If they're the same, it doesn't
	 * matter; we just mirror. If they're different non-edge-cases, you
	 * need a blend shader (don't do that). */
	
	if (is_edge_blendfactor(dst_factor)) {
		part.dominant = MALI_BLEND_DOM_SOURCE;
		part.nondominant_mode = MALI_BLEND_NON_ZERO;

		if (dst_factor == PIPE_BLENDFACTOR_ONE)
			part.clip_modifier = MALI_BLEND_MOD_DEST_ONE;
	} else if (is_edge_blendfactor(src_factor)) {
		part.dominant = MALI_BLEND_DOM_DESTINATION;
		part.nondominant_mode = MALI_BLEND_NON_ZERO;

		if (src_factor == PIPE_BLENDFACTOR_ONE)
			part.clip_modifier = MALI_BLEND_MOD_SOURCE_ONE;

	} else if (src_factor == dst_factor) {
		part.dominant = MALI_BLEND_DOM_DESTINATION; /* Arbitrary choice, but align with the blob until we understand more */
		part.nondominant_mode = MALI_BLEND_NON_MIRROR;
	} else {
		printf("Failed to find dominant factor?\n");
		return false;
	}

	unsigned in_dominant_factor =
		part.dominant == MALI_BLEND_DOM_SOURCE ? src_factor : dst_factor;

	if (part.clip_modifier == MALI_BLEND_MOD_NORMAL && in_dominant_factor == PIPE_BLENDFACTOR_ONE) {
		part.clip_modifier = part.dominant == MALI_BLEND_DOM_SOURCE ? MALI_BLEND_MOD_SOURCE_ONE : MALI_BLEND_MOD_DEST_ONE;
		in_dominant_factor = PIPE_BLENDFACTOR_ZERO;
	}

	bool invert_dominant = false;
	enum mali_dominant_factor dominant_factor;

	if (!trans_make_dominant_factor(in_dominant_factor, &dominant_factor, &invert_dominant))
		return false;

	part.dominant_factor = dominant_factor;
	part.complement_dominant = invert_dominant;

	/* Write out mode */
	memcpy(out, &part, sizeof(part));

	return true;
}

/* Create the descriptor for a fixed blend mode given the corresponding Gallium
 * state, if possible. Return true and write out the blend descriptor into
 * blend_equation. If it is not possible with the fixed function
 * representating, return false to handle degenerate cases with a blend shader
 */

static bool
trans_make_fixed_blend_mode(struct pipe_rt_blend_state *blend, struct mali_blend_equation *out)
{
	unsigned rgb_mode = 0;
	unsigned alpha_mode = 0;

	if (!trans_make_fixed_blend_part(
		blend->rgb_func, blend->rgb_src_factor, blend->rgb_dst_factor,
		&rgb_mode))
			return false;

	if (!trans_make_fixed_blend_part(
		blend->alpha_func, blend->alpha_src_factor, blend->alpha_dst_factor,
		&alpha_mode))
			return false;

	out->rgb_mode = rgb_mode;
	out->alpha_mode = alpha_mode;

	/* Gallium and Mali represent colour masks identically. XXX: Static assert for future proof */
	out->color_mask = blend->colormask;

	return true;
}

void
trans_default_shader_backend(struct panfrost_context *ctx)
{
	struct mali_fragment_core shader = {
		.alpha_coverage = ~MALI_ALPHA_COVERAGE(0.000000),
		.unknown2_3 = MALI_DEPTH_FUNC(MALI_FUNC_ALWAYS) | 0x3010,
		.stencil_mask_front = 0xFF,
		.stencil_mask_back = 0xFF,
		.unknown2_4 = MALI_NO_MSAA | 0x4f0,
		.stencil_front = {
			.mask = 0xFF,
			.func = MALI_FUNC_ALWAYS,
			.sfail = MALI_STENCIL_KEEP,
			.dpfail = MALI_STENCIL_KEEP,
			.dppass = MALI_STENCIL_KEEP,
		},
		.stencil_back = {
			.mask = 0xFF,
			.func = MALI_FUNC_ALWAYS,
			.sfail = MALI_STENCIL_KEEP,
			.dpfail = MALI_STENCIL_KEEP,
			.dppass = MALI_STENCIL_KEEP,
		},
	};

	struct pipe_rt_blend_state default_blend = {
		.rgb_func = PIPE_BLEND_ADD,
		.rgb_src_factor = PIPE_BLENDFACTOR_ONE,
		.rgb_dst_factor = PIPE_BLENDFACTOR_ZERO,

		.alpha_func = PIPE_BLEND_ADD,
		.alpha_src_factor = PIPE_BLENDFACTOR_ONE,
		.alpha_dst_factor = PIPE_BLENDFACTOR_ZERO,

		.colormask = PIPE_MASK_RGBA
	};

	if (!trans_make_fixed_blend_mode(&default_blend, &shader.blend_equation))
		printf("ERROR: Default shader backend must not trigger blend shader\n");

    memcpy(&ctx->fragment_shader_core, &shader, sizeof(shader));
}

/* Generates a vertex/tiler job. This is, in some sense, the heart of the
 * graphics command stream. It should be called once per draw, accordding to
 * presentations. Set is_tiler for "tiler" jobs (fragment shader jobs, but in
 * Mali parlance, "fragment" refers to framebuffer writeout). Clear it for
 * vertex jobs. */

static mali_ptr
trans_vertex_tiler_job(struct panfrost_context *ctx, bool is_tiler)
{
	/* Each draw call corresponds to two jobs, and we want to offset to leave room for the set-value job */
	int draw_job_index = 1 + (2 * ctx->draw_count);

    struct mali_job_descriptor_header job_1 = {
      .job_type = is_tiler ? JOB_TYPE_TILER : JOB_TYPE_VERTEX,
      .job_index = draw_job_index + (is_tiler ? 1 : 0),
    };

    /* XXX: What is this? */
    if (is_tiler)
	    job_1.unknown_flags = 1;

    /* Only tiler jobs have dependencies which are known at this point */

    if (is_tiler) {
	    /* Tiler jobs depend on vertex jobs */

	    job_1.job_dependency_index_1 = draw_job_index;

	    /* Tiler jobs also depend on the previous tiler job */

	    if (ctx->draw_count)
		    job_1.job_dependency_index_2 = draw_job_index - 1;
    } 

    struct mali_payload_vertex_tiler *payload =
	    is_tiler ? &ctx->payload_tiler : &ctx->payload_vertex;
   
    mali_ptr job_1_p = panfrost_upload(&ctx->cmdstream, &job_1, sizeof(job_1) - 4, true);
    panfrost_upload_sequential(&ctx->cmdstream, payload, sizeof(*payload));

    return job_1_p;
}

/* Generates a set value job. It's unclear what exactly this does, why it's
 * necessary, and when to call it. */

static mali_ptr
trans_set_value_job(struct panfrost_context *ctx)
{
	struct mali_job_descriptor_header job_0 = {
		.job_type = JOB_TYPE_SET_VALUE,
		.job_descriptor_size = 1,
		.job_index = 1 + (2 * ctx->draw_count),
	};

	struct mali_payload_set_value payload_0 = {
		.out = ctx->scratchpad.gpu + 0x6000,
		.unknown = 0x3,
	};

	mali_ptr job_0_p = panfrost_upload(&ctx->cmdstream, &job_0, sizeof(job_0), true);
	panfrost_upload_sequential(&ctx->cmdstream, &payload_0, sizeof(payload_0));

	return job_0_p;
}

/* Generate a fragment job. This should be called once per frame. (According to
 * presentations, this is supposed to correspond to eglSwapBuffers) */

static mali_ptr
trans_fragment_job(struct panfrost_context *ctx)
{
	/* The frame is complete and therefore the framebuffer descriptor is
	 * ready for linkage and upload */

	mali_ptr fbd = panfrost_upload(&ctx->cmdstream, &ctx->fragment_fbd, sizeof(ctx->fragment_fbd), false);

	/* Generate the fragment (frame) job */

	struct mali_job_descriptor_header header = {
		.job_type = JOB_TYPE_FRAGMENT,
		.job_index = 1,
	};

	struct mali_payload_fragment payload = {
		.min_tile_coord = MALI_COORDINATE_TO_TILE_MIN(0, 0),
		.max_tile_coord = MALI_COORDINATE_TO_TILE_MAX(ctx->width, ctx->height),
		.framebuffer = fbd | PANFROST_DEFAULT_FBD,
	};

	mali_ptr job_pointer = panfrost_upload(&ctx->cmdstream, &header, sizeof(header) - 4, true);
	panfrost_upload_sequential(&ctx->cmdstream, &payload, sizeof(payload));

	return job_pointer;
}

/* Go through dirty flags and actualise them in the cmdstream. */

static void
trans_emit_for_draw(struct panfrost_context *ctx)
{
	ctx->dirty &= ~PAN_DIRTY_DUMMY;

	if (ctx->dirty & PAN_DIRTY_VERT_BUF) {
		/* TODO: Only update the dirtied buffers */
		struct mali_attr attrs[PIPE_MAX_ATTRIBS];

		for (int i = 0; i < ctx->vertex->num_elements; ++i) {
			struct mali_attr_meta *hw = &ctx->vertex->hw[i];
			struct pipe_vertex_buffer *buf = &ctx->vertex_buffers[ctx->vertex->pipe[i].vertex_buffer_index];
			struct panfrost_resource *rsrc = (struct panfrost_resource *) (buf->buffer.resource);

			/* Offset vertex count by draw_start to make sure we upload enough */
			attrs[i] = trans_attr(sizeof(float), MALI_NEGATIVE(hw->nr_components), ctx->payload_vertex.draw_start + ctx->vertex_count);
			attrs[i].elements = panfrost_upload(&ctx->cmdstream, (uint8_t *) rsrc->cpu + buf->buffer_offset + ctx->vertex->pipe[i].src_offset, attrs[i].size, false) | 1;
		}
		
		ctx->payload_vertex.attributes = panfrost_upload(&ctx->cmdstream, attrs, ctx->vertex_buffer_count * sizeof(struct mali_attr), false);

		/* Dirtied attributes implies dirtied varyings */
		ctx->dirty &= ~PAN_DIRTY_VERT_BUF;
		ctx->dirty |= PAN_DIRTY_VARYINGS;
	}

	if (ctx->dirty & PAN_DIRTY_VARYINGS) {
		struct mali_attr varyings[PIPE_MAX_ATTRIBS];

		/* height of the gl_Position varying, which is always(?) last */
		int position_height = 0;

		for (int i = 0; i < ctx->varying_count; ++i) {
			int size = ctx->varyings_stride[i] * ctx->vertex_count;

			varyings[i].elements = (ctx->varying_mem.gpu + ctx->varying_height) | 1;
			varyings[i].size = size;
			varyings[i].stride = ctx->varyings_stride[i];

			/* Varyings appear to need 64-byte alignment */
			size += 64 - (size & 63);

			if ((i + 1) == ctx->varying_count)
				position_height = ctx->varying_height;

			ctx->varying_height += size;
		}

		mali_ptr varyings_p = panfrost_upload(&ctx->cmdstream, &varyings, ctx->varying_count * sizeof(struct mali_attr), false);
		ctx->payload_vertex.varyings = varyings_p;
		ctx->payload_tiler.varyings = varyings_p;
		ctx->payload_tiler.position_varying = ctx->varying_mem.gpu + position_height;

		ctx->dirty &= ~PAN_DIRTY_VARYINGS;
	}

	if (ctx->dirty & PAN_DIRTY_RASTERIZER) {
		ctx->payload_tiler.line_width = ctx->rasterizer->base.line_width;
		ctx->payload_tiler.gl_enables = ctx->rasterizer->tiler_gl_enables;

		trans_set_framebuffer_msaa(ctx, FORCE_MSAA || ctx->rasterizer->base.multisample);

		ctx->dirty &= ~PAN_DIRTY_RASTERIZER;
	}

	if (ctx->dirty & PAN_DIRTY_VS) {
		ctx->payload_vertex._shader_upper = panfrost_upload(&ctx->cmdstream, &ctx->vs->tripipe, sizeof(struct mali_tripipe), true) >> 4;
		panfrost_reserve(&ctx->cmdstream, sizeof(struct mali_fragment_core));

		/* Varying descriptor is tied to the vertex shader. Also the
		 * fragment shader, I suppose, but it's generated with the
		 * vertex shader so */

		trans_upload_varyings_descriptor(ctx);

		ctx->dirty &= ~PAN_DIRTY_VS;
	}

	if (ctx->dirty & PAN_DIRTY_FS) { 
		ctx->payload_tiler._shader_upper = panfrost_upload(&ctx->cmdstream, &ctx->fs->tripipe, sizeof(struct mali_tripipe), true) >> 4;
		panfrost_upload_sequential(&ctx->cmdstream, &ctx->fragment_shader_core, sizeof(struct mali_fragment_core));

		ctx->dirty &= ~PAN_DIRTY_FS;
	}

	if (ctx->dirty & PAN_DIRTY_VERTEX) {
		ctx->payload_vertex.attribute_meta = panfrost_upload(&
				ctx->cmdstream, &ctx->vertex->hw,
				sizeof(struct mali_attr_meta) * ctx->vertex->num_elements, false);

		ctx->dirty &= ~PAN_DIRTY_VERTEX;
	}

	if (ctx->dirty & PAN_DIRTY_VIEWPORT) {
		ctx->payload_tiler.viewport = panfrost_upload(&ctx->cmdstream, &ctx->viewport, sizeof(struct mali_viewport), false);

		ctx->dirty &= ~PAN_DIRTY_VIEWPORT;
	}

	for (int i = 0; i < PIPE_SHADER_TYPES; ++i) {
		struct panfrost_constant_buffer *buf = &ctx->constant_buffer[i];

		if (buf->dirty) {
			mali_ptr address = panfrost_upload(&ctx->cmdstream, buf->buffer, buf->size, false);
			
			switch (i) {
				case PIPE_SHADER_VERTEX:
					ctx->payload_vertex.uniforms = address;
					break;

				case PIPE_SHADER_FRAGMENT:
					ctx->payload_tiler.uniforms = address;
					break;

				default:
					printf("Unknown shader stage %d in uniform upload\n", i);
					break;
			}

			buf->dirty = 0;
		}
	}
}

/* Corresponds to exactly one draw, but does not submit anything */

void
trans_queue_draw(struct panfrost_context *ctx)
{
	/* TODO: Expand the array? */
	if (ctx->draw_count >= MAX_DRAW_CALLS) {
		printf("Job buffer overflow, ignoring draw\n");
		return;
	}

	/* Handle dirty flags now */
	trans_emit_for_draw(ctx);

	ctx->vertex_jobs[ctx->draw_count] = trans_vertex_tiler_job(ctx, false);
	ctx->tiler_jobs[ctx->draw_count] = trans_vertex_tiler_job(ctx, true);
	ctx->draw_count++;
}

/* At the end of the frame, the vertex and tiler jobs are linked together and
 * then the fragment job is plonked at the end. Set value job is first for
 * unknown reasons. */

#define JOB_DESC(ptr) ((struct mali_job_descriptor_header *) (uintptr_t) ptr)
static void
trans_link_job_pair(mali_ptr first, mali_ptr next)
{
	if (JOB_DESC(first)->job_descriptor_size)
		JOB_DESC(first)->next_job_64 = (u64) (uintptr_t) next;
	else
		JOB_DESC(first)->next_job_32 = (u32) (uintptr_t) next;
}

static void
trans_link_jobs(struct panfrost_context *ctx)
{
	if (ctx->draw_count) {
		/* Generate the set_value_job */
		ctx->set_value_job = trans_set_value_job(ctx);

		/* Have the first vertex job depend on the set value job */
		JOB_DESC(ctx->vertex_jobs[0])->job_dependency_index_1 = JOB_DESC(ctx->set_value_job)->job_index;

		/* SV -> V */
		trans_link_job_pair(ctx->set_value_job, ctx->vertex_jobs[0]);
	}

	/* V -> V/T ; T -> T/null */
	for (int i = 0; i < ctx->draw_count; ++i) {
		bool isLast = (i + 1) == ctx->draw_count;

		trans_link_job_pair(ctx->vertex_jobs[i], isLast ? ctx->tiler_jobs[0] : ctx->vertex_jobs[i + 1]);
		trans_link_job_pair(ctx->tiler_jobs[i], isLast ? 0 : ctx->tiler_jobs[i + 1]);
	}
}

/* Use to allocate atom numbers for jobs. We probably want to overhaul this in kernel space at some point. */
uint8_t atom_counter = 0;

static uint8_t
allocate_atom()
{
	atom_counter++;

	/* Workaround quirk where atoms must be strictly positive */

	if (atom_counter == 0)
		atom_counter++;

	return atom_counter;
}

/* The entire frame is in memory -- send it off to the kernel! */

static void
trans_submit_frame(struct panfrost_context *ctx)
{
	/* Edge case if screen is cleared and nothing else */
	bool has_draws = ctx->draw_count > 0;

	/* A number of jobs are batched -- this must be linked and cleared */
	trans_link_jobs(ctx);

	ctx->draw_count = 0;

	mali_external_resource framebuffer[] = {
		ctx->framebuffer.gpu | MALI_EXT_RES_ACCESS_EXCLUSIVE,
	};

	struct mali_jd_atom_v2 atoms[] = {
		{
			.jc = ctx->set_value_job,
			.atom_number = allocate_atom(),
			.compat_core_req = MALI_JD_REQ_CS | MALI_JD_REQ_T | MALI_JD_REQ_CF | MALI_JD_REQ_COHERENT_GROUP
		},
		{
			.jc = trans_fragment_job(ctx),
			.nr_ext_res = 1,
			.ext_res_list = framebuffer,
			.atom_number = allocate_atom(),
			.compat_core_req = MALI_JD_REQ_FS | MALI_JD_REQ_EXTERNAL_RESOURCES | MALI_JD_REQ_SKIP_CACHE_START
		},
	};

	struct mali_ioctl_job_submit submit = {
		.addr = atoms,
		.nr_atoms = 1,
		.stride = sizeof(struct mali_jd_atom_v2),
	};

	if (pandev_ioctl(ctx->fd, MALI_IOCTL_JOB_SUBMIT, &submit))
	    printf("Error submitting\n");

	/* Submit jobs seperately to workaround the missing tile issue? XXX
	 * FIXME when we redesign the character interface */

	struct mali_ioctl_job_submit submit2 = {
		.addr = atoms + 1,
		.nr_atoms = 1,
		.stride = sizeof(struct mali_jd_atom_v2),
	};

	if (pandev_ioctl(ctx->fd, MALI_IOCTL_JOB_SUBMIT, &submit2))
	    printf("Error submitting\n");
}

static void
panfrost_flush(
		struct pipe_context *pipe,
		struct pipe_fence_handle ** fence,
		unsigned flags)
{
	struct panfrost_context *ctx = panfrost_context(pipe);

	/* If there is nothing drawn, skip the frame */
	if (!ctx->draw_count && !(ctx->dirty & PAN_DIRTY_DUMMY)) return;

	/* Submit the frame itself */
	trans_submit_frame(ctx);

	/* Prepare for the next frame */
	trans_invalidate_frame(ctx);
	usleep(1000);

	/* Clear the kernel event buffer */
	uint8_t kernel_events[128];
	read(ctx->fd, kernel_events, 128);

	/* Display the frame in our cute little window */
#ifndef HAVE_DRI3
	slowfb_update((uint8_t*) ctx->framebuffer.cpu, ctx->width, ctx->height);
#endif
}	

#define DEFINE_CASE(c) case PIPE_PRIM_##c: return MALI_GL_##c;

static int
g2m_draw_mode(enum pipe_prim_type mode)
{
	switch(mode) {
		DEFINE_CASE(POINTS);
		DEFINE_CASE(LINES);
		DEFINE_CASE(LINE_LOOP);
		DEFINE_CASE(LINE_STRIP);
		DEFINE_CASE(TRIANGLES);
		DEFINE_CASE(TRIANGLE_STRIP);
		DEFINE_CASE(TRIANGLE_FAN);

		default:
			printf("Illegal draw mode %d\n", mode);
			return MALI_GL_LINE_LOOP;
	}
}

#undef DEFINE_CASE

static void
panfrost_draw_vbo(
		struct pipe_context *pipe,
		const struct pipe_draw_info *info)
{
	struct panfrost_context *ctx = panfrost_context(pipe);

	ctx->payload_vertex.draw_start = info->start;
	ctx->payload_tiler.draw_start = info->start;

	/* Draw mode is specified in the draw itself */
        ctx->payload_tiler.draw_mode = g2m_draw_mode(info->mode);

	ctx->vertex_count = info->count;

        ctx->payload_vertex.vertex_count = MALI_POSITIVE(ctx->vertex_count);
        ctx->payload_tiler.vertex_count = ctx->payload_vertex.vertex_count;

	if (info->index_size) {
		/* TODO: Indexed draws */
		printf("ERROR: No indexed draws supported yet\n");
		return;
	} else {
		/* Index count == vertex count, if no indexing is applied, as
		 * if it is internally indexed in the expected order */

		ctx->payload_tiler.index_count = ctx->payload_tiler.vertex_count;
	}

	/* Fire off the draw itself */
	trans_queue_draw(ctx);
}

/* CSO state */

static void
panfrost_generic_cso_delete(struct pipe_context *pctx, void *hwcso)
{
	free(hwcso);
}

static void*
panfrost_create_rasterizer_state(
		struct pipe_context *pctx,
		const struct pipe_rasterizer_state *cso)
{
	struct panfrost_rasterizer *so = CALLOC_STRUCT(panfrost_rasterizer);

	so->base = *cso;

	/* Bitmask, unknown meaning of the start value */
	so->tiler_gl_enables = 0x105;

	so->tiler_gl_enables |= MALI_GL_FRONT_FACE(
			cso->front_ccw ? MALI_GL_CCW : MALI_GL_CW);

	if (cso->cull_face & PIPE_FACE_FRONT)
		so->tiler_gl_enables |= MALI_GL_CULL_FACE_FRONT;

	if (cso->cull_face & PIPE_FACE_BACK)
		so->tiler_gl_enables |= MALI_GL_CULL_FACE_BACK;

	return so;
}

static void
panfrost_bind_rasterizer_state(
		struct pipe_context *pctx,
		void *hwcso)
{
	struct panfrost_context *ctx = panfrost_context(pctx);

	ctx->rasterizer = hwcso;
	ctx->dirty |= PAN_DIRTY_RASTERIZER;
}

#ifdef HAVE_DRI3
#include "util/u_format.h"
#endif

static void*
panfrost_create_vertex_elements_state(
		struct pipe_context *pctx,
		unsigned num_elements,
		const struct pipe_vertex_element *elements)
{
	struct panfrost_vertex_state *so = CALLOC_STRUCT(panfrost_vertex_state);

	so->num_elements = num_elements;
	memcpy(so->pipe, elements, sizeof(*elements) * num_elements);

	/* Create the packed hardware attribute meta now. TODO: Actually use
	 * the values from Gallium, don't hardcode. Requires understanding the
	 * structure a bit better, as well as having access to a Gallium
	 * utility function or two. */

	for (int i = 0; i < num_elements; ++i) {
		
#ifdef HAVE_DRI3
		enum pipe_format fmt = elements[i].src_format;
		int nr_components = util_format_get_nr_components(fmt);
#else
		int nr_components = 4; /* XXX */
#endif

		so->hw[i].index = i;
		so->hw[i].type = 7;
		so->hw[i].nr_components = MALI_POSITIVE(nr_components);
                so->hw[i].not_normalised = 1;
		so->hw[i].is_int_signed = 0;
                so->hw[i].unknown1 = 0x2a22;
                so->hw[i].unknown2 = 0x1;
                so->hw[i].unknown3 = 0x0;
	}

	return so;
}

static void
panfrost_bind_vertex_elements_state(
		struct pipe_context *pctx,
		void *hwcso)
{
	struct panfrost_context *ctx = panfrost_context(pctx);

	ctx->vertex = hwcso;
	ctx->dirty |= PAN_DIRTY_VERTEX;
}

static void *
panfrost_create_shader_state(
		struct pipe_context *pctx,
		const struct pipe_shader_state *cso)
{
	struct panfrost_shader_state *so = CALLOC_STRUCT(panfrost_shader_state);
	so->base = *cso;
	return so;
}

static void
panfrost_delete_shader_state(
		struct pipe_context *pctx,
		void *so)
{
	free(so);
}

static void
panfrost_bind_fs_state(
		struct pipe_context *pctx,
		void *hwcso)
{
	struct panfrost_context *ctx = panfrost_context(pctx);

	ctx->fs = hwcso;

	/* XXX TODO: Actual compilation pipeline */
	if (!ctx->fs->compiled) {
		panfrost_shader_compile(ctx, &ctx->fs->tripipe, NULL, JOB_TYPE_TILER);
		ctx->fs->compiled = true;
	}

	ctx->dirty |= PAN_DIRTY_FS;
}

static void
panfrost_bind_vs_state(
		struct pipe_context *pctx,
		void *hwcso)
{
	struct panfrost_context *ctx = panfrost_context(pctx);

	ctx->vs = hwcso;

	if (!ctx->vs->compiled) {
		panfrost_shader_compile(ctx, &ctx->vs->tripipe, NULL, JOB_TYPE_VERTEX);
		ctx->vs->compiled = true;
	}

	ctx->dirty |= PAN_DIRTY_VS;
}

static void
panfrost_set_vertex_buffers(
		struct pipe_context *pctx,
		unsigned start_slot,
		unsigned num_buffers,
		const struct pipe_vertex_buffer *buffers)
{
	struct panfrost_context *ctx = panfrost_context(pctx);
	assert(num_buffers <= PIPE_MAX_ATTRIBS);

	/* XXX: Dirty tracking? etc */

	size_t sz = sizeof(buffers[0]) * num_buffers;
	ctx->vertex_buffers = malloc(sz);
	ctx->vertex_buffer_count = num_buffers;
	memcpy(ctx->vertex_buffers, buffers, sz);

	ctx->dirty |= PAN_DIRTY_VERT_BUF;

#if 0
	struct panfrost_resource *rsrc = (struct panfrost_resource *) (buffers->buffer.resource);
#endif
}


static void
panfrost_set_constant_buffer(
		struct pipe_context *pctx,
		enum pipe_shader_type shader, uint index,
		const struct pipe_constant_buffer *buf)
{
	struct panfrost_context *ctx = panfrost_context(pctx);
	size_t sz = buf->buffer_size;

	/* Multiple constant buffers not yet supported */
	assert(index == 0);

	const void *cpu;

	struct panfrost_resource *rsrc = (struct panfrost_resource *) (buf->buffer);

	if (rsrc) {
		cpu = rsrc->cpu;
	} else if (buf->user_buffer) {
		cpu = buf->user_buffer;
	} else {
		printf("No constant buffer?\n");
		return;
	}

	/* Copy the constant buffer into the driver context and adjust dirty
	 * flags as necessary */

	struct panfrost_constant_buffer *pbuf = &ctx->constant_buffer[shader];

	pbuf->dirty = true;
	pbuf->size = sz;

	if (pbuf->buffer)
		free(pbuf->buffer);

	pbuf->buffer = malloc(sz);
	memcpy(pbuf->buffer, cpu, sz);
}

static void
panfrost_set_stencil_ref(
		struct pipe_context *pctx,
		const struct pipe_stencil_ref *ref)
{
	struct panfrost_context *ctx = panfrost_context(pctx);

	ctx->fragment_shader_core.stencil_front.ref = ref->ref_value[0];
	ctx->fragment_shader_core.stencil_back.ref = ref->ref_value[1];
}

/* TODO: Proper resource tracking depends on, well, proper resources. This
 * section will be woefully incomplete until we can sort out a proper DRM
 * driver. */

#ifdef HAVE_DRI3
#define UNUSED
#include "util/u_inlines.h"
#endif

struct pipe_resource *
panfrost_resource_create_front(struct pipe_screen *screen,
			       const struct pipe_resource *template,
			       const void *map_front_private)
{
	struct panfrost_resource *so = CALLOC_STRUCT(panfrost_resource);

	so->base = *template;
	so->base.screen = screen;
#ifdef HAVE_DRI3
	pipe_reference_init(&so->base.reference, 1);
#endif
	/* TODO: Textures n stuff */
	/* TODO: Allocate straight on the cmdstream for zero-copy operation */
	so->cpu = malloc(template->width0);

	return (struct pipe_resource *)so;
}

static struct pipe_resource *
panfrost_resource_create(struct pipe_screen *screen,
		const struct pipe_resource *templat)
{
	return panfrost_resource_create_front(screen, templat, NULL);
}

static void *
panfrost_transfer_map(struct pipe_context *pctx,
                      struct pipe_resource *resource,
                      unsigned level,
                      unsigned usage,  /* a combination of PIPE_TRANSFER_x */
                      const struct pipe_box *box,
                      struct pipe_transfer **out_transfer)
{
	struct pipe_transfer *transfer = CALLOC_STRUCT(pipe_transfer);
	transfer->resource = resource;
	transfer->level = level;
	transfer->usage = usage;
	transfer->box = *box;

	struct panfrost_resource *rsrc = (struct panfrost_resource *) resource;

	return ((uint8_t *) rsrc->cpu) + transfer->box.x;
}

static void
panfrost_transfer_unmap(struct pipe_context *pctx,
                       struct pipe_transfer *transfer)
{
	printf("Unmap trans\n");
	//free(transfer);
}

static void
trans_allocate_slab(struct panfrost_context *ctx,
		    struct panfrost_memory *mem,
		    size_t pages,
		    bool mapped,
		    bool same_va,
		    int extra_flags,
		    int commit_count,
		    int extent)
{
	int flags = MALI_MEM_PROT_CPU_RD | MALI_MEM_PROT_CPU_WR | MALI_MEM_PROT_GPU_RD | MALI_MEM_PROT_GPU_WR;

	flags |= extra_flags;

	/* w+x are mutually exclusive */
	if (extra_flags & MALI_MEM_PROT_GPU_EX)
		flags &= ~MALI_MEM_PROT_GPU_WR;

	if (same_va)
		flags |= MALI_MEM_SAME_VA;

	if (commit_count || extent)
		pandev_general_allocate(ctx->fd, pages, commit_count, extent, flags, &mem->gpu);
	else
		pandev_standard_allocate(ctx->fd, pages, flags, &mem->gpu);

	mem->size = pages * 4096;

	if (mapped)
		mem->cpu = mmap64(NULL, mem->size, 3, 1, ctx->fd, mem->gpu);

	mem->stack_bottom = 0;
}

/* Setups a framebuffer, either by itself (with the independent slow-fb
 * interface) or from an existing pointer (for shared memory, from the winsys)
 * */

void
trans_setup_framebuffer(struct panfrost_context *ctx, uint32_t *addr, int width, int height)
{
	ctx->width = width;
	ctx->height = height;
	ctx->bytes_per_pixel = 4; /* RGBA32 */

#ifdef HAVE_DRI3
	/* drisw rounds the stride */
	int rw = 16.0 * (int) ceil((float) ctx->width / 16.0);
#else
	int rw = ctx->width;
#endif

	ctx->stride = rw * ctx->bytes_per_pixel;

	size_t framebuffer_sz = ctx->stride * ctx->height;

	/* Unclear why framebuffers are offset like this */
	int offset = (ctx->width - 256) * ctx->bytes_per_pixel;

	if (addr) {
		ctx->framebuffer.cpu = addr;
	} else {
		posix_memalign((void **) &ctx->framebuffer.cpu, CACHE_LINE_SIZE, framebuffer_sz + offset);
		slowfb_init((uint8_t*) (ctx->framebuffer.cpu + offset), ctx->width, ctx->height);
	}

	struct mali_mem_import_user_buffer framebuffer_handle = { .ptr = (uint64_t) (uintptr_t) ctx->framebuffer.cpu, .length = framebuffer_sz + offset };

	struct mali_ioctl_mem_import framebuffer_import = {
		.phandle = (uint64_t) (uintptr_t) &framebuffer_handle,
		.type = MALI_MEM_IMPORT_TYPE_USER_BUFFER,
		.flags = MALI_MEM_PROT_CPU_RD | MALI_MEM_PROT_CPU_WR | MALI_MEM_PROT_GPU_RD | MALI_MEM_PROT_GPU_WR,
	};

	pandev_ioctl(ctx->fd, MALI_IOCTL_MEM_IMPORT, &framebuffer_import);

	ctx->framebuffer.gpu = framebuffer_import.gpu_va;
	ctx->framebuffer.size = framebuffer_sz;
}

static void
trans_setup_hardware(struct panfrost_context *ctx)
{
	ctx->fd = pandev_open();

	trans_allocate_slab(ctx, &ctx->cmdstream, 8*64, true, true, 0, 0, 0);
	trans_allocate_slab(ctx, &ctx->scratchpad, 16, true, true, 0, 0, 0);
	trans_allocate_slab(ctx, &ctx->varying_mem, 32, false, true, MALI_MEM_COHERENT_LOCAL, 0, 0);
	trans_allocate_slab(ctx, &ctx->shaders, 4096, true, false, MALI_MEM_PROT_GPU_EX, 1, 0);
	trans_allocate_slab(ctx, &ctx->tiler_heap, 32768, false, false, MALI_MEM_GROW_ON_GPF, 1, 128);

#ifndef HAVE_DRI3
	/* Mesa allocates the framebuffer for us, but when standalone we handle it ourselves */
	trans_setup_framebuffer(ctx, NULL, 1366, 768);
#endif
}

/* New context creation, which also does hardware initialisation since I don't
 * know the better way to structure this :smirk: */

struct pipe_context *
panfrost_create_context(struct pipe_screen *screen, void *priv, unsigned flags)
{
	screen->resource_create = panfrost_resource_create;
	screen->resource_create_front = panfrost_resource_create_front;

	struct panfrost_context *ctx = CALLOC_STRUCT(panfrost_context);
	memset(ctx, 0, sizeof(*ctx));
	struct pipe_context *gallium = (struct pipe_context *) ctx;

	gallium->transfer_map = panfrost_transfer_map;
	gallium->transfer_unmap = panfrost_transfer_unmap;

	gallium->flush = panfrost_flush;
	gallium->clear = panfrost_clear;
	gallium->draw_vbo = panfrost_draw_vbo;

	gallium->set_vertex_buffers = panfrost_set_vertex_buffers;
	gallium->set_constant_buffer = panfrost_set_constant_buffer;

	gallium->set_stencil_ref = panfrost_set_stencil_ref;

	gallium->create_rasterizer_state = panfrost_create_rasterizer_state;
	gallium->bind_rasterizer_state = panfrost_bind_rasterizer_state;
	gallium->delete_rasterizer_state = panfrost_generic_cso_delete;

	gallium->create_vertex_elements_state = panfrost_create_vertex_elements_state;
	gallium->bind_vertex_elements_state = panfrost_bind_vertex_elements_state;
	gallium->delete_vertex_elements_state = panfrost_generic_cso_delete;

	gallium->create_fs_state = panfrost_create_shader_state;
	gallium->delete_fs_state = panfrost_delete_shader_state;
	gallium->bind_fs_state = panfrost_bind_fs_state;

	gallium->create_vs_state = panfrost_create_shader_state;
	gallium->delete_vs_state = panfrost_delete_shader_state;
	gallium->bind_vs_state = panfrost_bind_vs_state;

	/* Prepare for render! */
	trans_setup_hardware(ctx);
	trans_invalidate_frame(ctx);
	trans_default_shader_backend(ctx);

	return gallium;
}
