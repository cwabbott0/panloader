/*
 * © Copyright 2017 The Panfrost Community
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>

#include <panloader-util.h>
#include <mali-ioctl.h>
#include "pandev.h"
#include <mali-job.h>

#include <sys/user.h>

/* From Linux arch/arm/include/asm/page.h */

#define PAGE_SHIFT	12
#ifndef PAGE_SIZE
# define PAGE_SIZE 	(1 << PAGE_SHIFT)
#endif
#ifndef PAGE_MASK
# define PAGE_MASK 	(~(PAGE_SIZE - 1))
#endif

/* From the kernel module */

#define MALI_MEM_MAP_TRACKING_HANDLE (3ull << 12)
#define MALI_CONTEXT_CREATE_FLAG_NONE 0

int
pandev_ioctl(int fd, unsigned long request, void *args)
{
	union mali_ioctl_header *h = args;
	int rc;

	h->id = ((_IOC_TYPE(request) & 0xF) << 8) | _IOC_NR(request);

	rc = ioctl(fd, request, args);
	if (rc)
		return rc;

	switch (h->rc) {
	case MALI_ERROR_NONE:              return 0;
	case MALI_ERROR_FUNCTION_FAILED:   return -EINVAL;
	case MALI_ERROR_OUT_OF_MEMORY:     return -ENOMEM;
	case MALI_ERROR_OUT_OF_GPU_MEMORY: return -ENOSPC;
	default:			   return -EINVAL;
	}
}

static int
pandev_get_driver_version(int fd, unsigned *major, unsigned *minor)
{
	int rc;

	struct mali_ioctl_get_version args = {
		.major = 10,
		.minor = 4
	};

	/* So far this seems to be the only ioctl that uses 0x80 for dir */
	rc = pandev_ioctl(fd, MALI_IOCTL_GET_VERSION, &args);
	if (rc)
		return rc;

	*major = args.major;
	*minor = args.minor;

	return 0;
}

static int
pandev_set_flags(int fd)
{
	struct mali_ioctl_set_flags args = {
		.create_flags = MALI_CONTEXT_CREATE_FLAG_NONE
	};

	return pandev_ioctl(fd, MALI_IOCTL_SET_FLAGS, &args);
}

int
pandev_general_allocate(int fd, int va_pages, int commit_pages, int extent, int flags, u64 *out)
{
	struct mali_ioctl_mem_alloc args = {
		.va_pages = va_pages,
		.commit_pages = commit_pages,
		.extent = extent,
		.flags = flags
	};

	int rc = pandev_ioctl(fd, MALI_IOCTL_MEM_ALLOC, &args);

	if (rc)
		return rc;

	*out = args.gpu_va;

	return 0;
}

int
pandev_standard_allocate(int fd, int va_pages, int flags, u64 *out)
{
	return pandev_general_allocate(fd, va_pages, va_pages, 0, flags, out);
}

/**
 * Low-level open call, used by the main pandev_open
 */

int
pandev_raw_open()
{
	return open("/dev/mali0", O_RDWR | O_NONBLOCK | O_CLOEXEC);
}

/* The Memmap Tracking Handle is necessary to be mapped for the kernel
 * driver to be happy. It is still unclear why this is mapped or what
 * we are supposed to dowith the mapped region. TODO
 */

u8*
pandev_map_mtp(int fd)
{
	return mmap(NULL, PAGE_SIZE, PROT_NONE, MAP_SHARED, fd, MALI_MEM_MAP_TRACKING_HANDLE);
}

/**
 * Open the device file for communicating with the mali kernelspace driver,
 * and make sure it's a version of the kernel driver we're familiar with.
 *
 * Returns: fd on success, exits on failure
 */
int
pandev_open()
{
	int fd = pandev_raw_open(),
	    rc;
	unsigned major, minor;

	if (fd < 0)
		goto fail;

	rc = pandev_get_driver_version(fd, &major, &minor);
	if (rc)
		goto fail;

	if (pandev_map_mtp(fd) == MAP_FAILED)
		goto fail;

	rc = pandev_set_flags(fd);
	if (rc)
		goto fail;

	return fd;

fail:
	printf("Error opening kernel driver\n");
	exit(0);
}
