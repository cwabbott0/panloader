/*
 * © Copyright 2017-2018 The Panfrost Community
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#include "panwrap.h"
#include <mali-ioctl.h>
#include <mali-job.h>
#include <stdio.h>
#include <memory.h>

#include "panwrap-shader.h"

#define MEMORY_PROP(obj, p) {\
	char *a = pointer_as_memory_reference(obj->p); \
	panwrap_prop("%s = %s", #p, a); \
	free(a); \
}

#define DYN_MEMORY_PROP(obj, no, p) { \
	if (obj->p) \
		panwrap_prop("%s = %s_%d_p", #p, #p, no); \
}

#define FLAG_INFO(flag) { MALI_GL_##flag, "MALI_GL_" #flag }
static const struct panwrap_flag_info gl_enable_flag_info[] = {
	FLAG_INFO(CULL_FACE_FRONT),
	FLAG_INFO(CULL_FACE_BACK),
	{}
};
#undef FLAG_INFO

#define FLAG_INFO(flag) { MALI_CLEAR_##flag, "MALI_CLEAR_" #flag }
static const struct panwrap_flag_info clear_flag_info[] = {
	FLAG_INFO(FAST),
	FLAG_INFO(SLOW),
	FLAG_INFO(SLOW_STENCIL),
	{}
};
#undef FLAG_INFO

#define FLAG_INFO(flag) { MALI_MASK_##flag, "MALI_MASK_" #flag }
static const struct panwrap_flag_info mask_flag_info[] = {
	FLAG_INFO(R),
	FLAG_INFO(G),
	FLAG_INFO(B),
	FLAG_INFO(A),
	{}
};
#undef FLAG_INFO

#define FLAG_INFO(flag) { MALI_##flag, "MALI_" #flag }
static const struct panwrap_flag_info u3_flag_info[] = {
	FLAG_INFO(HAS_MSAA),
	FLAG_INFO(CAN_DISCARD),
	FLAG_INFO(HAS_BLEND_SHADER),
	FLAG_INFO(DEPTH_TEST),
	{}
};

static const struct panwrap_flag_info u4_flag_info[] = {
	FLAG_INFO(NO_MSAA),
	FLAG_INFO(NO_DITHER),
	FLAG_INFO(DEPTH_RANGE_A),
	FLAG_INFO(DEPTH_RANGE_B),
	FLAG_INFO(STENCIL_TEST),
	FLAG_INFO(SAMPLE_ALPHA_TO_COVERAGE_NO_BLEND_SHADER),
	{}
};
#undef FLAG_INFO

#define FLAG_INFO(flag) { MALI_FRAMEBUFFER_##flag, "MALI_FRAMEBUFFER_" #flag }
static const struct panwrap_flag_info u2_flag_info[] = {
	FLAG_INFO(MSAA_A),
	FLAG_INFO(MSAA_B),
	FLAG_INFO(MSAA_8),
	{}
};
#undef FLAG_INFO

extern char* replace_fragment;
extern char* replace_vertex;

static char *panwrap_job_type_name(enum mali_job_type type)
{
#define DEFINE_CASE(name) case JOB_TYPE_ ## name: return "JOB_TYPE_" #name
	switch (type) {
	DEFINE_CASE(NULL);
	DEFINE_CASE(SET_VALUE);
	DEFINE_CASE(CACHE_FLUSH);
	DEFINE_CASE(COMPUTE);
	DEFINE_CASE(VERTEX);
	DEFINE_CASE(TILER);
	DEFINE_CASE(FUSED);
	DEFINE_CASE(FRAGMENT);
	case JOB_NOT_STARTED:
		return "NOT_STARTED";
	default:
		panwrap_log("Warning! Unknown job type %x\n", type);
		return "!?!?!?";
	}
#undef DEFINE_CASE
}

static char *panwrap_gl_mode_name(enum mali_gl_mode mode)
{
#define DEFINE_CASE(name) case MALI_ ## name: return "MALI_" #name
	switch(mode) {
	DEFINE_CASE(GL_POINTS);
	DEFINE_CASE(GL_LINES);
	DEFINE_CASE(GL_TRIANGLES);
	DEFINE_CASE(GL_TRIANGLE_STRIP);
	DEFINE_CASE(GL_TRIANGLE_FAN);
	DEFINE_CASE(GL_LINE_STRIP);
	DEFINE_CASE(GL_LINE_LOOP);
	default: return "MALI_GL_TRIANGLES /* XXX: Unknown GL mode, check dump */";
	}
#undef DEFINE_CASE
}

#define DEFINE_CASE(name) case MALI_FUNC_ ## name: return "MALI_FUNC_" #name
static char *panwrap_func_name(enum mali_func mode)
{
	switch(mode) {
	DEFINE_CASE(NEVER);
	DEFINE_CASE(LESS);
	DEFINE_CASE(EQUAL);
	DEFINE_CASE(LEQUAL);
	DEFINE_CASE(GREATER);
	DEFINE_CASE(NOTEQUAL);
	DEFINE_CASE(GEQUAL);
	DEFINE_CASE(ALWAYS);
	default: return "MALI_FUNC_NEVER /* XXX: Unknown function, check dump */";
	}
}
#undef DEFINE_CASE

/* Why is this duplicated? Who knows... */
#define DEFINE_CASE(name) case MALI_ALT_FUNC_ ## name: return "MALI_ALT_FUNC_" #name
static char *panwrap_alt_func_name(enum mali_alt_func mode)
{
	switch(mode) {
	DEFINE_CASE(NEVER);
	DEFINE_CASE(LESS);
	DEFINE_CASE(EQUAL);
	DEFINE_CASE(LEQUAL);
	DEFINE_CASE(GREATER);
	DEFINE_CASE(NOTEQUAL);
	DEFINE_CASE(GEQUAL);
	DEFINE_CASE(ALWAYS);
	default: return "MALI_FUNC_NEVER /* XXX: Unknown function, check dump */";
	}
}
#undef DEFINE_CASE



#define DEFINE_CASE(name) case MALI_STENCIL_ ## name: return "MALI_STENCIL_" #name
static char *panwrap_stencil_op_name(enum mali_stencil_op op)
{
	switch(op) {
	DEFINE_CASE(KEEP);
	DEFINE_CASE(REPLACE);
	DEFINE_CASE(ZERO);
	DEFINE_CASE(INVERT);
	DEFINE_CASE(INCR_WRAP);
	DEFINE_CASE(DECR_WRAP);
	DEFINE_CASE(INCR);
	DEFINE_CASE(DECR);
	default: return "MALI_STENCIL_KEEP /* XXX: Unknown stencil op, check dump */";
	}
}

#undef DEFINE_CASE

#define DEFINE_CASE(name) case MALI_CHANNEL_## name: return "MALI_CHANNEL_" #name
static char *panwrap_channel_name(enum mali_stencil_op op)
{
	switch(op) {
	DEFINE_CASE(RED);
	DEFINE_CASE(GREEN);
	DEFINE_CASE(BLUE);
	DEFINE_CASE(ALPHA);
	DEFINE_CASE(ZERO);
	DEFINE_CASE(ONE);
	DEFINE_CASE(RESERVED_0);
	DEFINE_CASE(RESERVED_1);
	default: return "MALI_CHANNEL_ZERO /* XXX: Unknown channel, check dump */";
	}
}
#undef DEFINE_CASE

#define DEFINE_CASE(name) case MALI_WRAP_## name: return "MALI_WRAP_" #name
static char *panwrap_wrap_mode_name(enum mali_wrap_mode op)
{
	switch(op) {
	DEFINE_CASE(REPEAT);
	DEFINE_CASE(CLAMP_TO_EDGE);
	DEFINE_CASE(CLAMP_TO_BORDER);
	DEFINE_CASE(MIRRORED_REPEAT);
	default: return "MALI_WRAP_REPEAT /* XXX: Unknown wrap mode, check dump */";
	}
}
#undef DEFINE_CASE

static inline char *panwrap_decode_fbd_type(enum mali_fbd_type type)
{
	if (type == MALI_SFBD)      return "SFBD";
	else if (type == MALI_MFBD) return "MFBD";
	else return "WTF!?";
}

static bool
panwrap_deduplicate(const struct panwrap_mapped_memory *mem, uint64_t gpu_va, const char *name, int number)
{
	if (mem->touched[(gpu_va - mem->gpu_va) / sizeof(uint32_t)]) {
		/* XXX: Is this correct? */
		panwrap_log("mali_ptr %s_%d_p = %s_%d_p;\n", name, number, name, number - 1);
		return true;
	}

	return false;
}

static void
panwrap_replay_sfbd(uint64_t gpu_va, int job_no)
{
	struct panwrap_mapped_memory *mem = panwrap_find_mapped_gpu_mem_containing(gpu_va);
	const struct mali_single_framebuffer *PANWRAP_PTR_VAR(s, mem, (mali_ptr) gpu_va);

	/* FBDs are frequently duplicated, so watch for this */
	//if (panwrap_deduplicate(mem, gpu_va, "framebuffer", job_no)) return;

	panwrap_log("struct mali_single_framebuffer framebuffer_%d = {\n", job_no);
	panwrap_indent++;

	panwrap_prop("unknown1 = 0x%" PRIx32, s->unknown1);
	panwrap_prop("flags = 0x%" PRIx32, s->flags);
	panwrap_prop("heap_free_address = 0x%" PRIx64, s->heap_free_address);

	panwrap_log(".unknown2 = ");
	panwrap_log_decoded_flags(u2_flag_info, s->unknown2);
	panwrap_log_cont(",\n");

	panwrap_prop("width = MALI_POSITIVE(%" PRId16 ")", s->width + 1);
	panwrap_prop("height = MALI_POSITIVE(%" PRId16 ")", s->height + 1);

	MEMORY_PROP(s, framebuffer_end);
	panwrap_prop("negative_stride = %d", s->negative_stride);

	/* Earlier in the actual commandstream -- right before width -- but we
	 * delay to flow nicer */

	panwrap_log(".clear_flags = ");
	panwrap_log_decoded_flags(clear_flag_info, s->clear_flags);
	panwrap_log_cont(",\n");

	if (s->depth_buffer | s->depth_buffer_enable) {
		MEMORY_PROP(s, depth_buffer);
		panwrap_prop("depth_buffer_enable = %s", DS_ENABLE(s->depth_buffer_enable));
	}

	if (s->stencil_buffer | s->stencil_buffer_enable) {
		MEMORY_PROP(s, stencil_buffer);
		panwrap_prop("stencil_buffer_enable = %s", DS_ENABLE(s->stencil_buffer_enable));
	}

	if (s->clear_color_1 | s->clear_color_2 | s->clear_color_3 | s->clear_color_4) {
		panwrap_prop("clear_color_1 = 0x%" PRIx32, s->clear_color_1);
		panwrap_prop("clear_color_2 = 0x%" PRIx32, s->clear_color_2);
		panwrap_prop("clear_color_3 = 0x%" PRIx32, s->clear_color_3);
		panwrap_prop("clear_color_4 = 0x%" PRIx32, s->clear_color_4);
	}

	if (s->clear_depth_1 != 0 || s->clear_depth_2 != 0 || s->clear_depth_3 != 0 || s->clear_depth_4 != 0) {
		panwrap_prop("clear_depth_1 = %f", s->clear_depth_1);
		panwrap_prop("clear_depth_2 = %f", s->clear_depth_2);
		panwrap_prop("clear_depth_3 = %f", s->clear_depth_3);
		panwrap_prop("clear_depth_4 = %f", s->clear_depth_4);
	}

	if (s->clear_stencil) {
		panwrap_prop("clear_stencil = 0x%x", s->clear_stencil);
	}

	MEMORY_PROP(s, unknown_address_0);
	MEMORY_PROP(s, unknown_address_1);
	MEMORY_PROP(s, unknown_address_2);

	panwrap_prop("resolution_check = 0x%" PRIx32, s->resolution_check);
	panwrap_prop("unknown9 = 0x%" PRIx32, s->unknown9);

	MEMORY_PROP(s, tiler_heap_free);
	MEMORY_PROP(s, tiler_heap_end);

	panwrap_indent--;
	panwrap_log("};\n");

	int zero_sum_pun = 0;
	zero_sum_pun += s->zero1;
	zero_sum_pun += s->zero2;
	zero_sum_pun += s->zero4;
	for (int i = 0; i < sizeof(s->zero3)/sizeof(s->zero3[0]); ++i) zero_sum_pun += s->zero3[i];
	for (int i = 0; i < sizeof(s->zero6)/sizeof(s->zero6[0]); ++i) zero_sum_pun += s->zero6[i];

	if (zero_sum_pun)
		panwrap_msg("Zero sum tripped (%d), replay may be wrong\n", zero_sum_pun);

	TOUCH(mem, (mali_ptr) gpu_va, *s, "framebuffer", job_no, true);
}

void panwrap_replay_attributes(const struct panwrap_mapped_memory *mem,
			       mali_ptr addr, int job_no, int count, bool varying)
{
	char *prefix = varying ? "varyings" : "attributes";

	/* Varyings in particular get duplicated between parts of the job */
	if (panwrap_deduplicate(mem, addr, prefix, job_no)) return;

	struct mali_attr *attr = panwrap_fetch_gpu_mem(mem, addr, sizeof(struct mali_attr) * count); 

	char base[128];
	snprintf(base, sizeof(base), "%s_data_%d", prefix, job_no);

	for (int i = 0; i < count; ++i) {
		mali_ptr raw_elements = attr[i].elements & ~3;

		size_t vertex_count;
		size_t component_count;

		/* gl_VertexID and gl_InstanceID do not have elements to
		 * decode; we would crash if we tried */

		if (!varying && i < MALI_SPECIAL_ATTRIBUTE_BASE && 0) {
			/* TODO: Attributes are not necessarily float32 vectors in general;
			 * decoding like this without snarfing types from the shader is unsafe all things considered */

			panwrap_msg("i: %d\n", i);
			panwrap_msg("attr: %llx\n", raw_elements);
			struct panwrap_mapped_memory *l_mem = panwrap_find_mapped_gpu_mem_containing(raw_elements);
			float *buffer = panwrap_fetch_gpu_mem(l_mem, raw_elements, attr[i].size);

			vertex_count = attr[i].size / attr[i].stride;
			component_count = attr[i].stride / sizeof(float);

			panwrap_log("float %s_%d[] = {\n", base, i);

#define MIN(a, b) ((a > b) ? b : a)

#if 0
			panwrap_indent++;
			for (int row = 0; row < MIN(vertex_count, 16); row++) {
				panwrap_log_empty();

				for (int i = 0; i < component_count; i++)
					panwrap_log_cont("%ff, ", buffer[i]);

				panwrap_log_cont("\n");

				buffer += component_count;
			}
#endif
			panwrap_indent--;
			panwrap_log("};\n");

			TOUCH_LEN(mem, raw_elements, attr[i].size, base, i, true);
		} else {
			/* TODO: Allocate space for varyings dynamically? */

			char *a = pointer_as_memory_reference(raw_elements);
			panwrap_log("mali_ptr %s_%d_p = %s;\n", base, i, a);
			free(a);
		}
	}

	panwrap_log("struct mali_attr %s_%d[] = {\n", prefix, job_no);
	panwrap_indent++;

	for (int i = 0; i < count; ++i) {
		panwrap_log("{\n");
		panwrap_indent++;

		panwrap_prop("elements = (%s_%d_p) | %d", base, i, (int) (attr[i].elements & 3));
		panwrap_prop("stride = 0x%" PRIx32, attr[i].stride);
		panwrap_prop("size = 0x%" PRIx32, attr[i].size);
		panwrap_indent--;
		panwrap_log("}, \n");
	}

	panwrap_indent--;
	panwrap_log("};\n");

	TOUCH_LEN(mem, addr, sizeof(*attr) * count, prefix, job_no, true);
}

static mali_ptr
panwrap_replay_shader_address(const char *name, mali_ptr ptr)
{
	/* TODO: Decode flags */
	mali_ptr shader_ptr = ptr & ~15;

	char *a = pointer_as_memory_reference(shader_ptr);
	panwrap_prop("%s = (%s) | %d", name, a, (int) (ptr & 15));
	free(a);

	return shader_ptr;
}

static void
panwrap_replay_stencil(const char *name, const struct mali_stencil_test *stencil)
{
	const char *func = panwrap_func_name(stencil->func);
	const char *sfail = panwrap_stencil_op_name(stencil->sfail);
	const char *dpfail = panwrap_stencil_op_name(stencil->dpfail);
	const char *dppass = panwrap_stencil_op_name(stencil->dppass);

	if (stencil->zero)
		panwrap_msg("Stencil zero tripped: %X\n", stencil->zero);

	panwrap_log(".stencil_%s = {\n", name);
	panwrap_indent++;
	panwrap_prop("ref = %d", stencil->ref);
	panwrap_prop("mask = 0x%02X", stencil->mask);
	panwrap_prop("func = %s", func);
	panwrap_prop("sfail = %s", sfail);
	panwrap_prop("dpfail = %s", dpfail);
	panwrap_prop("dppass = %s", dppass);
	panwrap_indent--;
	panwrap_log("},\n");
}

static void
panwrap_replay_blend_equation(const struct mali_blend_equation *blend)
{
	if (blend->zero1 || blend->padding)
		panwrap_msg("Blend zero tripped: %X, %X\n", blend->zero1, blend->padding);

	panwrap_log(".blend_equation = {\n");
	panwrap_indent++;

	panwrap_prop("rgb_mode = 0x%X", blend->rgb_mode);
	panwrap_prop("alpha_mode = 0x%X", blend->alpha_mode);

	panwrap_log(".color_mask = ");
	panwrap_log_decoded_flags(mask_flag_info, blend->color_mask);
	panwrap_log_cont(",\n");

	panwrap_indent--;
	panwrap_log("},\n");
}

static void
panwrap_replay_attribute_meta(int job_no, int count, const struct mali_payload_vertex_tiler *v, bool varying)
{
	char base[128];
	char *prefix = varying ? "varying" : "attribute";
	snprintf(base, sizeof(base), "%s_meta", prefix); 

	panwrap_log("struct mali_attr_meta %s_%d[] = {\n", base, job_no);
	panwrap_indent++;

	struct mali_attr_meta *attr_meta;
	mali_ptr p = varying ? (v->unknown6 & ~0xF) : v->attribute_meta;

	struct panwrap_mapped_memory *attr_mem = panwrap_find_mapped_gpu_mem_containing(p);

	for (int i = 0; i < count; ++i, p += sizeof(struct mali_attr_meta)) {
		attr_meta = panwrap_fetch_gpu_mem(attr_mem, p,
						  sizeof(*attr_mem));

		panwrap_log("{\n");
		panwrap_indent++;
		panwrap_prop("index = %d", attr_meta->index);
		panwrap_prop("type = %d", attr_meta->type);
		panwrap_prop("nr_components = MALI_POSITIVE(%d)", MALI_NEGATIVE(attr_meta->nr_components));

		/* TODO: Dissect correctly */
		panwrap_prop("is_int_signed = %d", attr_meta->is_int_signed);

		panwrap_prop("not_normalised = %d", attr_meta->not_normalised);
		panwrap_prop("unknown1 = 0x%" PRIx64, (u64) attr_meta->unknown1);
		panwrap_prop("unknown2 = 0x%" PRIx64, (u64) attr_meta->unknown2);
		panwrap_prop("unknown3 = 0x%" PRIx64, (u64) attr_meta->unknown3);
		panwrap_indent--;
		panwrap_log("},\n");

	}

	panwrap_indent--;
	panwrap_log("};\n");

	panwrap_msg("not working but touching %d * %d?\n", sizeof(struct mali_attr_meta), count);
	TOUCH_LEN(attr_mem, p, sizeof(struct mali_attr_meta) * count, base, job_no, true);
}

static int
panwrap_replay_vertex_or_tiler_job(const struct mali_job_descriptor_header *h,
					const struct panwrap_mapped_memory *mem,
					mali_ptr payload, int job_no)
{
	struct mali_payload_vertex_tiler *PANWRAP_PTR_VAR(v, mem, payload);
	struct mali_shader_meta *meta;
	struct panwrap_mapped_memory *attr_mem;
	mali_ptr shader_meta_ptr = (u64) (uintptr_t) (v->_shader_upper << 4);

	/* TODO: Isn't this an -M-FBD? What's the difference? */
	panwrap_replay_sfbd((u64) (uintptr_t) v->framebuffer, job_no);

	int varying_count = 0, attribute_count = 0, uniform_count = 0;

	if (shader_meta_ptr) {
		struct panwrap_mapped_memory *smem = panwrap_find_mapped_gpu_mem_containing(shader_meta_ptr);
		struct mali_shader_meta *PANWRAP_PTR_VAR(s, smem, shader_meta_ptr);

		panwrap_log("struct mali_shader_meta shader_meta_%d = {\n", job_no);
		panwrap_indent++;

		/* Shader meta contains metadata for the entire shader core */
		struct mali_tripipe *t = &s->tripipe;
		struct mali_fragment_core *f = &s->fragment_core;

		/* Save for dumps */
		attribute_count = t->attribute_count;
		varying_count = t->varying_count;
		uniform_count = t->uniform_count; 

		panwrap_log(".tripipe = {\n");
		panwrap_indent++;

		mali_ptr shader_ptr = panwrap_replay_shader_address("shader", t->shader);

		if (t->zero1)
			panwrap_msg("XXX shader zero tripped\n");

		panwrap_prop("attribute_count = %" PRId16, t->attribute_count);
		panwrap_prop("varying_count = %" PRId16, t->varying_count);
		panwrap_prop("uniform_count = %" PRId16, t->uniform_count);
		panwrap_prop("work_count = %" PRId16, t->work_count);
		panwrap_prop("unknown1 = %s0x%" PRIx32,
				t->unknown1 & MALI_NO_ALPHA_TO_COVERAGE ? "MALI_NO_ALPHA_TO_COVERAGE | " : "",
				t->unknown1 & ~MALI_NO_ALPHA_TO_COVERAGE);
		panwrap_prop("unknown2 = 0x%" PRIx32, t->unknown2);

		panwrap_indent--;
		panwrap_log("},\n");

		panwrap_log(".fragment_core = {\n");
		panwrap_indent++;


		if (f->depth_units || f->depth_factor) {
			panwrap_prop("depth_units = MALI_NEGATIVE(%f)", f->depth_units - 1.0f);
			panwrap_prop("depth_factor = %f", f->depth_factor);
		}

		bool invert_alpha_coverage = f->alpha_coverage & 0xFFF0;
		uint16_t inverted_coverage = invert_alpha_coverage ? ~f->alpha_coverage : f->alpha_coverage;

		panwrap_prop("alpha_coverage = %sMALI_ALPHA_COVERAGE(%f)",
				invert_alpha_coverage ? "~" : "",
				MALI_GET_ALPHA_COVERAGE(inverted_coverage));

		panwrap_log(".unknown2_3 = ");

		int unknown2_3 = f->unknown2_3;
		int unknown2_4 = f->unknown2_4;
		
		/* We're not quite sure what these flags mean without the depth test, if anything */

		if (unknown2_3 & (MALI_DEPTH_TEST | MALI_DEPTH_FUNC_MASK)) {
			const char *func = panwrap_func_name(MALI_GET_DEPTH_FUNC(unknown2_3));
			unknown2_3 &= ~MALI_DEPTH_FUNC_MASK;

			panwrap_log_cont("MALI_DEPTH_FUNC(%s) | ", func);
		}


		panwrap_log_decoded_flags(u3_flag_info, unknown2_3);
		panwrap_log_cont(",\n");

		panwrap_prop("stencil_mask_front = 0x%02X", f->stencil_mask_front);
		panwrap_prop("stencil_mask_back = 0x%02X", f->stencil_mask_back);

		panwrap_log(".unknown2_4 = ");
		panwrap_log_decoded_flags(u4_flag_info, unknown2_4);
		panwrap_log_cont(",\n");

		panwrap_replay_stencil("front", &f->stencil_front);
		panwrap_replay_stencil("back", &f->stencil_back);

		panwrap_prop("unknown2_7 = 0x%" PRIx32, f->unknown2_7);
		panwrap_prop("unknown2_8 = 0x%" PRIx32, f->unknown2_8);

		bool blend_shader = false;

		if (f->unknown2_3 & MALI_HAS_BLEND_SHADER) {
			blend_shader = true;
			panwrap_replay_shader_address("blend_shader", f->blend_shader);
		} else {
			panwrap_replay_blend_equation(&f->blend_equation);
		}

		panwrap_indent--;
		panwrap_log("}\n");

		panwrap_indent--;
		panwrap_log("};\n");

		TOUCH(smem, shader_meta_ptr, *meta, "shader_meta", job_no, false);

		panwrap_shader_disassemble(shader_ptr, job_no, h->job_type);

		if (blend_shader)
			panwrap_shader_disassemble(f->blend_shader & ~0xF, job_no, h->job_type);

	} else
		panwrap_msg("<no shader>\n");

	if (v->viewport) {
		struct panwrap_mapped_memory *fmem = panwrap_find_mapped_gpu_mem_containing(v->viewport);
		struct mali_viewport *PANWRAP_PTR_VAR(f, fmem, v->viewport);

		panwrap_log("struct mali_viewport viewport_%d = {\n", job_no);
		panwrap_indent++;
		panwrap_log(".floats = {\n");
		panwrap_indent++;

		for (int i = 0; i < sizeof(f->floats) / sizeof(f->floats[0]); i += 2)
			panwrap_log("%ff, %ff,\n", f->floats[i], f->floats[i + 1]);

		panwrap_indent--;
		panwrap_log("},\n");

		panwrap_prop("depth_range_n = %f", f->depth_range_n);
		panwrap_prop("depth_range_f = %f", f->depth_range_f);

		/* Only the higher coordinates are MALI_POSITIVE scaled */

		panwrap_prop("viewport0 = { %d, %d }",
				f->viewport0[0], f->viewport0[1]);

		panwrap_prop("viewport1 = { MALI_POSITIVE(%d), MALI_POSITIVE(%d) }",
				f->viewport1[0] + 1, f->viewport1[1] + 1);

		panwrap_indent--;
		panwrap_log("};\n");

		TOUCH(fmem, v->viewport, *f, "viewport", job_no, true);
	}

	if (v->attribute_meta) {
		panwrap_replay_attribute_meta(job_no, attribute_count, v, false);

		attr_mem = panwrap_find_mapped_gpu_mem_containing(v->attributes);
		panwrap_replay_attributes(attr_mem, v->attributes, job_no, attribute_count, false);
	}

	/* Varyings are encoded like attributes but not actually sent; we just
	 * pass a zero buffer with the right stride/size set, (or whatever)
	 * since the GPU will write to it itself */

	if (v->varyings) {
		attr_mem = panwrap_find_mapped_gpu_mem_containing(v->varyings);

		/* Number of descriptors depends on whether there are
		 * non-internal varyings */

		panwrap_replay_attributes(attr_mem, v->varyings, job_no, varying_count > 1 ? 2 : 1, true);
	}

	/* XXX: This entire block is such a hack... where are uniforms configured exactly? */

	if (v->uniforms) {
		int rows = uniform_count, width = 4;
		size_t sz = rows * width * sizeof(float);

		struct panwrap_mapped_memory *uniform_mem = panwrap_find_mapped_gpu_mem_containing(v->uniforms);
		panwrap_fetch_gpu_mem(uniform_mem, v->uniforms, sz);
		float *PANWRAP_PTR_VAR(uniforms, uniform_mem, v->uniforms);

		panwrap_log("float uniforms_%d[] = {\n", job_no);

		panwrap_indent++;
		for (int row = 0; row < rows; row++) {
			panwrap_log_empty();

			for (int i = 0; i < width; i++)
				panwrap_log_cont("%ff, ", uniforms[i]);

			panwrap_log_cont("\n");

			uniforms += width;
		}
		panwrap_indent--;
		panwrap_log("};\n");

		TOUCH_LEN(mem, v->uniforms, sz, "uniforms", job_no, true);
	}

	if (v->unknown1) {
		struct panwrap_mapped_memory *umem = panwrap_find_mapped_gpu_mem_containing(v->unknown1);

		if (umem) {
			u64 *PANWRAP_PTR_VAR(u, umem, v->unknown1);

			mali_ptr ptr = *u >> 8;
			uint8_t flags = *u & 0xFF;

			/* Points to... a buffer of zeroes in the same region?
			 * *shrug* Deliberate 0 length so we don't memset
			 * anything out */
			
			panwrap_log("u32 inner_unknown1_%d = 0; /* XXX */\n", job_no);
			TOUCH_LEN(umem, ptr, 0, "inner_unknown1", job_no, true);

			panwrap_log("u64 unknown1_%d = ((inner_unknown1_%d_p) << 8) | %d;\n", job_no, job_no, flags);
			TOUCH(umem, v->unknown1, u64, "unknown1", job_no, true);
		}
	}

	if (v->unknown6) {
		struct panwrap_mapped_memory *umem = panwrap_find_mapped_gpu_mem_containing(v->unknown6);

		if (umem) {
			struct mali_unknown6 *PANWRAP_PTR_VAR(u, umem, v->unknown6 & ~0xF);

			panwrap_log("struct mali_unknown6 unknown6_%d = {\n", job_no);
			panwrap_indent++;

			panwrap_prop("unknown0 = 0x%" PRIx64, u->unknown0);
			panwrap_prop("unknown1 = 0x%" PRIx64, u->unknown1);


			panwrap_indent--;
			panwrap_log("};\n");

			TOUCH(umem, v->unknown6 & ~0xF, *u, "unknown6", job_no, false);

			panwrap_replay_attribute_meta(job_no, varying_count, v, true);
		}
	}

	/* Just a pointer to... another pointer >_< */

	if (v->texture_trampoline) {
		struct panwrap_mapped_memory *mmem = panwrap_find_mapped_gpu_mem_containing(v->texture_trampoline);

		if (mmem) {
			mali_ptr *PANWRAP_PTR_VAR(u, mmem, v->texture_trampoline);

			char *a = pointer_as_memory_reference(*u);
			panwrap_log("uint64_t texture_trampoline_%d = %s;", job_no, a);
			free(a);

			TOUCH(mmem, v->texture_trampoline, *u, "texture_trampoline", job_no, true);

			/* Now, finally, descend down into the texture descriptor */
			struct panwrap_mapped_memory *tmem = panwrap_find_mapped_gpu_mem_containing(*u);

			if (tmem) {
				struct mali_texture_descriptor *PANWRAP_PTR_VAR(t, tmem, *u);

				panwrap_log("struct mali_texture_descriptor texture_descriptor_%d = {\n", job_no);
				panwrap_indent++;

				panwrap_prop("width = MALI_POSITIVE(%" PRId16 ")", t->width + 1);
				panwrap_prop("height = MALI_POSITIVE(%" PRId16 ")", t->height + 1);

				panwrap_prop("unknown1 = 0x%" PRIx32, t->unknown1);
				
				/* TODO: I don't understand how this works at all yet */
				panwrap_prop("format1 = 0x%" PRIx32, t->format1);

				panwrap_prop("swizzle_r = %s", panwrap_channel_name(t->swizzle_r));
				panwrap_prop("swizzle_g = %s", panwrap_channel_name(t->swizzle_g));
				panwrap_prop("swizzle_b = %s", panwrap_channel_name(t->swizzle_b));
				panwrap_prop("swizzle_a = %s", panwrap_channel_name(t->swizzle_a));

				if (t->swizzle_zero) {
					/* Shouldn't happen */
					panwrap_msg("Swizzle zero tripped but replay will be fine anyway");
					panwrap_prop("swizzle_zero = %d", t->swizzle_zero);
				}

				panwrap_prop("unknown3 = 0x%" PRIx32, t->unknown3);

				panwrap_prop("unknown5 = 0x%" PRIx32, t->unknown5);
				panwrap_prop("unknown6 = 0x%" PRIx32, t->unknown6);
				panwrap_prop("unknown7 = 0x%" PRIx32, t->unknown7);

				MEMORY_PROP(t, swizzled_bitmap_0);
				MEMORY_PROP(t, swizzled_bitmap_1);

				panwrap_indent--;
				panwrap_log("};\n");

				TOUCH(tmem, *u, *t, "texture_descriptor", job_no, false);
			}
		}
	}

	if (v->sampler_descriptor) {
		struct panwrap_mapped_memory *smem = panwrap_find_mapped_gpu_mem_containing(v->sampler_descriptor);

		if (smem) {
			struct mali_sampler_descriptor *PANWRAP_PTR_VAR(s, smem, v->sampler_descriptor);

			panwrap_log("struct mali_sampler_descriptor sampler_descriptor_%d = {\n", job_no);
			panwrap_indent++;

			/* Only the lower two bits are understood right now; the rest we display as hex */
			panwrap_log(".filter_mode = MALI_GL_TEX_MIN(%s) | MALI_GL_TEX_MAG(%s) | 0x%" PRIx32",\n",
				       	MALI_FILTER_NAME(s->filter_mode & MALI_GL_TEX_MIN_MASK),
				       	MALI_FILTER_NAME(s->filter_mode & MALI_GL_TEX_MAG_MASK),
					s->filter_mode & ~3);

			panwrap_prop("unknown1 = 0x%" PRIx32, s->unknown1);

			panwrap_prop("wrap_s = %s", panwrap_wrap_mode_name(s->wrap_s));
			panwrap_prop("wrap_t = %s", panwrap_wrap_mode_name(s->wrap_t));
			panwrap_prop("wrap_r = %s", panwrap_wrap_mode_name(s->wrap_r));

			panwrap_prop("compare_func = %s", panwrap_alt_func_name(s->compare_func));

			if (s->zero || s->zero2) {
				panwrap_msg("Zero tripped\n");
				panwrap_prop("zero = 0x%X, 0x%X\n", s->zero, s->zero2);
			}

			panwrap_prop("unknown2 = %d", s->unknown2);

			panwrap_prop("border_color = { %f, %f, %f, %f }",
					s->border_color[0],
					s->border_color[1],
					s->border_color[2],
					s->border_color[3]);

			panwrap_indent--;
			panwrap_log("};\n");

			TOUCH(smem, v->sampler_descriptor, *s, "sampler_descriptor", job_no, false);
		}
	}

	if (v->indices) {
		struct panwrap_mapped_memory *imem = panwrap_find_mapped_gpu_mem_containing(v->indices);

		if (imem) {
			/* Indices are literally just a u32 array :) */

			uint32_t *PANWRAP_PTR_VAR(indices, imem, v->indices);

			panwrap_log("uint32_t indices_%d[] = {\n", job_no);
			panwrap_indent++;

			for(int i = 0; i < (v->index_count + 1); i += 3)
				panwrap_log("%d, %d, %d,\n",
						indices[i],
						indices[i + 1],
						indices[i + 2]);

			panwrap_indent--;
			panwrap_log("};\n");

			TOUCH_LEN(imem, v->indices, sizeof(uint32_t) * (v->index_count + 1), "indices", job_no, false);
		}
	}


	panwrap_log("struct mali_payload_vertex_tiler payload_%d = {\n", job_no);
	panwrap_indent++;

	panwrap_prop("line_width = %ff", v->line_width);
	panwrap_prop("vertex_count = MALI_POSITIVE(%" PRId32 ")", v->vertex_count + 1);
	panwrap_prop("unk1 = 0x%" PRIx32, v->unk1);
	panwrap_prop("draw_start = 0x%" PRIx32, v->draw_start);

	panwrap_prop("unknown_draw = 0x%" PRIx32, v->unknown_draw);

	if (h->job_type == JOB_TYPE_TILER) {
		panwrap_prop("draw_mode = %s", panwrap_gl_mode_name(v->draw_mode));
	} else {
		panwrap_prop("draw_mode = 0x%" PRIx32, v->draw_mode);
	}

	/* Index count only exists for tiler jobs anyway */ 

	if (v->index_count)
		panwrap_prop("index_count = MALI_POSITIVE(%" PRId32 ")", v->index_count + 1);

	uint32_t remaining_gl_enables = v->gl_enables;

	panwrap_log(".gl_enables = ");
	
	if (h->job_type == JOB_TYPE_TILER) {
		panwrap_log_cont("MALI_GL_FRONT_FACE(MALI_GL_%s) | ",
		    v->gl_enables & MALI_GL_FRONT_FACE(MALI_GL_CW) ? "CW" : "CCW");

		remaining_gl_enables &= ~(MALI_GL_FRONT_FACE(1));
	}

	panwrap_log_decoded_flags(gl_enable_flag_info, remaining_gl_enables);

	panwrap_log_cont(",\n");

	if (h->job_type == JOB_TYPE_VERTEX && v->index_count)
		panwrap_msg("Warning: index count set in vertex job\n");

	if (v->zero0 | v->zero1 | v->zero3 | v->zero5 | v->zero6) {
		panwrap_msg("Zero tripped\n");
		panwrap_prop("zero0 = 0x%" PRIx32, v->zero0);
		panwrap_prop("zero1 = 0x%" PRIx32, v->zero1);
		panwrap_prop("zero3 = 0x%" PRIx32, v->zero3);
		panwrap_prop("zero5 = 0x%" PRIx32, v->zero5);
		panwrap_prop("zero6 = 0x%" PRIx32, v->zero6);
	}

	DYN_MEMORY_PROP(v, job_no, indices);
	//DYN_MEMORY_PROP(v, job_no, unknown0);
	DYN_MEMORY_PROP(v, job_no, unknown1); 
	DYN_MEMORY_PROP(v, job_no, texture_trampoline);
	DYN_MEMORY_PROP(v, job_no, sampler_descriptor);
	DYN_MEMORY_PROP(v, job_no, uniforms);
	DYN_MEMORY_PROP(v, job_no, attributes); 
	DYN_MEMORY_PROP(v, job_no, attribute_meta); 
	DYN_MEMORY_PROP(v, job_no, varyings); 
	//DYN_MEMORY_PROP(v, job_no, unknown6);
	DYN_MEMORY_PROP(v, job_no, viewport);
	DYN_MEMORY_PROP(v, job_no, framebuffer);

	MEMORY_PROP(v, position_varying);

	panwrap_prop("_shader_upper = (shader_meta_%d_p) >> 4", job_no);
	panwrap_prop("flags = %d", v->flags); 

	if (v->unknown6)
		panwrap_prop("unknown6 = (unknown6_%d_p) | 0x%X", job_no, v->unknown6 & 0xF);

	panwrap_indent--;
	panwrap_log("};\n");

	return sizeof(*v);
}

static int panwrap_replay_fragment_job(const struct panwrap_mapped_memory *mem,
					mali_ptr payload, int job_no)
{
	const struct mali_payload_fragment *PANWRAP_PTR_VAR(s, mem, payload);

	bool fbd_dumped = false;

	if ((s->framebuffer & FBD_TYPE) == MALI_SFBD) {
		/* Only SFBDs are understood, not MFBDs. We're speculating,
		 * based on the versioning, kernel code, etc, that the
		 * difference is between Single FrameBuffer Descriptor and
		 * Multiple FrmaeBuffer Descriptor; the change apparently lines
		 * up with multi-framebuffer support being added (T7xx onwards,
		 * including Gxx). In any event, there's some field shuffling
		 * that we haven't looked into yet. */

		panwrap_replay_sfbd(s->framebuffer & FBD_MASK, job_no);
		fbd_dumped = true;
	}

	uintptr_t p = (uintptr_t) s->framebuffer & FBD_MASK;

	panwrap_log("struct mali_payload_fragment payload_%d = {\n", job_no);
	panwrap_indent++;

	if (s->zero)
		panwrap_msg("ZT\n");

	/* See the comments by the macro definitions for mathematical context
	 * on why this is so weird */

	if (MALI_TILE_COORD_FLAGS(s->max_tile_coord) || MALI_TILE_COORD_FLAGS(s->min_tile_coord))
		panwrap_msg("Tile coordinate flag missed, replay wrong\n");

	panwrap_prop("min_tile_coord = MALI_COORDINATE_TO_TILE_MIN(%d, %d)",
			MALI_TILE_COORD_X(s->min_tile_coord) << MALI_TILE_SHIFT,
			MALI_TILE_COORD_Y(s->min_tile_coord) << MALI_TILE_SHIFT);

	panwrap_prop("max_tile_coord = MALI_COORDINATE_TO_TILE_MAX(%d, %d)",
			(MALI_TILE_COORD_X(s->max_tile_coord) + 1) << MALI_TILE_SHIFT,
			(MALI_TILE_COORD_Y(s->max_tile_coord) + 1) << MALI_TILE_SHIFT);

	/* If the FBD was just decoded, we can refer to it by pointer. If not,
	 * we have to fallback on offsets. */

	const char *fbd_type = s->framebuffer & MALI_MFBD ? "MALI_MFBD" : "MALI_SFBD";

	if (fbd_dumped)
		panwrap_prop("framebuffer = framebuffer_%d_p | %s", job_no, fbd_type);
	else
		panwrap_prop("framebuffer = %s | %s", pointer_as_memory_reference(p), fbd_type);

	panwrap_indent--;
	panwrap_log("};\n");

	return sizeof(*s);
}

static int job_descriptor_number = 0;

int panwrap_replay_jc(mali_ptr jc_gpu_va)
{
	struct mali_job_descriptor_header *h;

	int start_number = 0;

	bool first = true;
	bool last_size;

	do {
		struct panwrap_mapped_memory *mem =
			panwrap_find_mapped_gpu_mem_containing(jc_gpu_va);

		void *payload;

		h = PANWRAP_PTR(mem, jc_gpu_va, typeof(*h));

		int offset = h->job_descriptor_size == MALI_JOB_32 ? 4 : 0;
		mali_ptr payload_ptr = jc_gpu_va + sizeof(*h) - offset;

		payload = panwrap_fetch_gpu_mem(mem, payload_ptr,
						MALI_PAYLOAD_SIZE);

		int job_no = job_descriptor_number++;

		if (first)
			start_number = job_no;

		panwrap_log("struct mali_job_descriptor_header job_%d = {\n", job_no);
		panwrap_indent++;

		panwrap_prop("job_type = %s", panwrap_job_type_name(h->job_type));

		/* Save for next job fixing */
		last_size = h->job_descriptor_size;

		if (h->job_descriptor_size)
			panwrap_prop("job_descriptor_size = %d", h->job_descriptor_size);

		if (h->exception_status)
			panwrap_prop("exception_status = %d", h->exception_status);

		if (h->first_incomplete_task)
			panwrap_prop("first_incomplete_task = %d", h->first_incomplete_task);

		if (h->fault_pointer)
			panwrap_prop("fault_pointer = 0x%" PRIx64, h->fault_pointer);

		if (h->job_barrier)
			panwrap_prop("job_barrier = %d", h->job_barrier);

		panwrap_prop("job_index = %d", h->job_index);

		if (h->unknown_flags)
			panwrap_prop("unknown_flags = %d", h->unknown_flags);

		if (h->job_dependency_index_1)
			panwrap_prop("job_dependency_index_1 = %d", h->job_dependency_index_1);

		if (h->job_dependency_index_2)
			panwrap_prop("job_dependency_index_2 = %d", h->job_dependency_index_2);

		panwrap_indent--;
		panwrap_log("};\n");

		/* Do not touch the field yet -- decode the payload first, and
		 * don't touch that either. This is essential for the uploads
		 * to occur in sequence and therefore be dynamically allocated
		 * correctly. Do note the size, however, for that related
		 * reason. */

		int payload_size = 0;

		switch (h->job_type) {
		case JOB_TYPE_SET_VALUE:
			{
				struct mali_payload_set_value *s = payload;

				panwrap_log("struct mali_payload_set_value payload_%d = {\n", job_no);
				panwrap_indent++;
				MEMORY_PROP(s, out);
				panwrap_prop("unknown = 0x%" PRIX64, s->unknown);
				panwrap_indent--;
				panwrap_log("};\n");

				payload_size = sizeof(*s);

				break;
			}
		case JOB_TYPE_TILER:
		case JOB_TYPE_VERTEX:
			payload_size = panwrap_replay_vertex_or_tiler_job(h, mem, payload_ptr, job_no);

			break;
		case JOB_TYPE_FRAGMENT:
			payload_size = panwrap_replay_fragment_job(mem, payload_ptr, job_no);
			break;
		default:
			break;
		}

		/* Touch the job descriptor fields, careful about 32/64-bit */
		TOUCH_JOB_HEADER(mem, jc_gpu_va, sizeof(*h), offset, job_no);

		/* Touch the payload immediately after, sequentially */
		TOUCH_SEQUENTIAL(mem, payload_ptr, payload_size, "payload", job_no);

		/* Handle linkage */

		if (!first) {
			panwrap_log("((struct mali_job_descriptor_header *) (uintptr_t) job_%d_p)->", job_no - 1);

			if (last_size)
				panwrap_log_cont("next_job_64 = job_%d_p;\n\n", job_no);
			else
				panwrap_log_cont("next_job_32 = (u32) (uintptr_t) job_%d_p;\n\n", job_no);
		}

		first = false;


	} while ((jc_gpu_va = h->job_descriptor_size ? h->next_job_64 : h->next_job_32));

	return start_number;
}

void panwrap_replay_soft_replay_payload(mali_ptr jc_gpu_va, int job_no)
{
	struct mali_jd_replay_payload *v;

	struct panwrap_mapped_memory *mem =
		panwrap_find_mapped_gpu_mem_containing(jc_gpu_va);

	v = PANWRAP_PTR(mem, jc_gpu_va, typeof(*v));

	panwrap_log("struct mali_jd_replay_payload soft_replay_payload_%d = {\n", job_no);
	panwrap_indent++;

	MEMORY_PROP(v, tiler_jc_list);
	MEMORY_PROP(v, fragment_jc);
	MEMORY_PROP(v, tiler_heap_free);

	panwrap_prop("fragment_hierarchy_mask = 0x%" PRIx32, v->fragment_hierarchy_mask);
	panwrap_prop("tiler_hierarchy_mask = 0x%" PRIx32, v->tiler_hierarchy_mask);
	panwrap_prop("hierarchy_default_weight = 0x%" PRIx32, v->hierarchy_default_weight);

	panwrap_log(".tiler_core_req = ");
	if (v->tiler_core_req)
		ioctl_log_decoded_jd_core_req(v->tiler_core_req);
	else
		panwrap_log_cont("0");
	panwrap_log_cont(",\n");

	panwrap_log(".fragment_core_req = ");
	if (v->fragment_core_req)
		ioctl_log_decoded_jd_core_req(v->fragment_core_req);
	else
		panwrap_log_cont("0");
	panwrap_log_cont(",\n");

	panwrap_indent--;
	panwrap_log("};\n");

	TOUCH(mem, jc_gpu_va, *v, "soft_replay_payload", job_no, false);
}

int panwrap_replay_soft_replay(mali_ptr jc_gpu_va)
{
	struct mali_jd_replay_jc *v;
	int start_no;
	bool first = true;

	do {
		struct panwrap_mapped_memory *mem =
			panwrap_find_mapped_gpu_mem_containing(jc_gpu_va);

		v = PANWRAP_PTR(mem, jc_gpu_va, typeof(*v));

		int job_no = job_descriptor_number++;

		if (first)
			start_no = job_no;

		first = false;

		panwrap_log("struct mali_jd_replay_jc job_%d = {\n", job_no);
		panwrap_indent++;

		MEMORY_PROP(v, next);
		MEMORY_PROP(v, jc);

		panwrap_indent--;
		panwrap_log("};\n");

		panwrap_replay_soft_replay_payload(jc_gpu_va /* + sizeof(struct mali_jd_replay_jc) */, job_no);

		TOUCH(mem, jc_gpu_va, *v, "job", job_no, false);
	} while ((jc_gpu_va = v->next));

	return start_no;
}
